import React, { useState, useEffect } from "react";
import PaymentProcessorList from "../components/payment/PaymentProcessor";
import VisaCard from "../components/payment/VisaCard";
// import OrangeMoney from "../components/payment/OrangeMoney";
import MtnMoney from "../components/payment/MtnMoney";
import OrangeMoney from "../components/payment/OrangeMoney";

export default usePaymentMethod = (data) => {
  const [paymentMethod, setPaymentMethod] = useState("");
  function changePaymentMethod(method, data) {
    setPaymentMethod(method);
  }
  const [modalContent, setModalContent] = useState(
    <PaymentProcessorList changePaymentMethod={changePaymentMethod} />
  );
  useEffect(() => {
    if (paymentMethod == "om") {
      setModalContent(<OrangeMoney data={data} />);
    } else if (paymentMethod == "mm") {
      setModalContent(<MtnMoney data={data} />);
    } else if (paymentMethod == "card") {
      setModalContent(<VisaCard data={data} />);
    } else {
      setModalContent(
        <PaymentProcessorList changePaymentMethod={changePaymentMethod} />
      );
    }
  }, [paymentMethod]);

  return { modalContent, changePaymentMethod };
};
