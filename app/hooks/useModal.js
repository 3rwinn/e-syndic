import { useState } from "react";

export default useModal = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [modalContent, setModalContent] = useState(null);
  function toggleModal(title, content) {
    if (title && content) {
      setModalTitle(title);
      setModalContent(content);
    }
    setModalVisible(!modalVisible);
  }
  function close() {
    setModalVisible(false);
  }

  function setVisible(bool) {
    setModalVisible(bool)
  }

  return { modalVisible, modalTitle, modalContent, toggleModal, close, setVisible };
};
