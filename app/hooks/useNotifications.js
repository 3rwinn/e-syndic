import { useEffect } from "react";
import * as Notifications from "expo-notifications";

// import expoPushTokensApi from "../api/expoPushTokens";
import authApi from "../api/auth";
import useAuth from "../auth/useAuth";
import navigation from "../navigation/rootNavigation";
// import * as Device from "expo-device";
import Constants from "expo-constants";

export const useNotifications = () => {

  Notifications.setNotificationHandler({
    handleNotification: async () => ({
      shouldShowAlert: true,
      shouldPlaySound: true,
      shouldSetBadge: false,
    }),
  });

  const { user } = useAuth();
  useEffect(() => {
    if (user) {
      registerForPushNotifications();
      Notifications.addNotificationResponseReceivedListener((notif) => {
        const notifData = notif.notification;

        const datas = notifData.request.content.data;
        const module = datas.module;

        if (module === "demandes de services") {
          navigation.navigate("Conciergerie");
        } else if (module === "evenements") {
          navigation.navigate("Évènements");
        } else if (module === "incidents") {
          navigation.navigate("Gestion des incidents");
        } else if (module === "messages") {
          navigation.navigate("Messagerie interne");
        } else if (module === "charges") {
          navigation.navigate("Mes charges");
        } else if (module === "note d'information") {
          navigation.navigate("Note d'information")
        }

        // logger.save(notif);
        // navigation.navigate("Mes charges");
      });
      // if (notificationListener) Notifications.addListener(notificationListener);
    }
  }, [user]);

  const registerForPushNotifications = async () => {
    try {
      const { status: existingStatus } =
        await Notifications.getPermissionsAsync();

      let finalStatus = existingStatus;

      if (existingStatus !== "granted") {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }

      if (finalStatus !== "granted" || !user) {
        console.log(
          "Permission not granted to get push token for push notification!"
        );
        return;
      }

      const projectId =
        Constants?.expoConfig?.extra?.eas?.projectId ??
        Constants?.easConfig?.projectId;
      if (!projectId) {
        console.log("Project ID not found");
      }

      const token = await Notifications.getExpoPushTokenAsync();
      const result = await authApi.saveToken(user.id, token.data);

      console.log("res", result.data);
    } catch (e) {
      console.log("error_push", e);
    }

    // try {
    //   // const user = await authStorage.getUser();
    //   const permission = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    //   if (!permission.granted || !user) return;

    //   const token = await Notifications.getExpoPushTokenAsync();
    //   const result = await authApi.saveToken(user.id, token.data);

    //   console.log("result", result.data);
    //   // console.log("le token qui s'en va", token);
    //   // logger.save(`TOKEN: ${token.data}`);
    // } catch (error) {
    //   console.log(error); // ajouter le logger
    // }
  };
};
