import { useEffect, useState } from "react";
const useData = (dataFetched, dataWanted) => {
  const [data, setData] = useState(null);
  useEffect(() => {
    if (dataWanted) {
      setData(dataWanted);
    }
  }, [dataFetched]);

  return { data };
};
export default useData;
