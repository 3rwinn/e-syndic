import { useState, useEffect } from "react";
import paymentApi from "../api/payment";
import PaymentStorage from "../auth/storage";
import useApi from "./useApi";
import useData from "./useData";

const usePaymentCheck = () => {
  const [lastPaymentData, setLastPaymentData] = useState(null);
  const [isCheckDone, setCheckDone] = useState(false);
  const resetLastPayment = async () => {
    const lastOne = await PaymentStorage.getLastPayment();
    if (lastOne) setLastPaymentData(lastOne);
  };
  const checkLastPaymentApi = useApi(paymentApi.checkPaymenWithMtn);

  const doTheCheck = async () => {
    await resetLastPayment();
    if (lastPaymentData !== null) {
      checkLastPaymentApi.request(lastPaymentData);
    }
  };

  useEffect(() => {
    if (checkLastPaymentApi.data.datas) {
      setCheckDone(true);
      PaymentStorage.removeLastPayment();
    }
  }, [checkLastPaymentApi.data]);

  return { doTheCheck, isCheckDone };
};
export default usePaymentCheck;
