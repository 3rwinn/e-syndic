import * as SecureStore from "expo-secure-store";

const key = "auth_user";

const storeUser = async (user) => {
  try {
    await SecureStore.setItemAsync(key, JSON.stringify(user));
  } catch (error) {
    console.log("Erreur lors de l'enregistrement de l'user", error);
  }
};

const getUser = async () => {
  try {
    const user_string = await SecureStore.getItemAsync(key);
    return JSON.parse(user_string);
  } catch (error) {
    console.log("Erreur lors de la recuperation de l'user", error);
  }
};

const removeUser = async () => {
  try {
    await SecureStore.deleteItemAsync(key);
  } catch (error) {
    console.log("Erreur lors de la suppression de l'user", error);
  }
};

// last payment
const storeLastPayment = async (payment) => {
  try {
    await SecureStore.setItemAsync("last_payment", JSON.stringify(payment));
    console.log("on ma appellé 1")

  } catch (error) {
    console.log("Erreur lors de l'enregistrement du last_payment", error);
  }
};

const getLastPayment = async () => {
  try {
    const last_payment = await SecureStore.getItemAsync("last_payment");
    return JSON.parse(last_payment);
  } catch (error) {
    console.log("Erreur lors de la recuperation du last_payment", error);
  }
};

const removeLastPayment = async () => {
  try {
    await SecureStore.deleteItemAsync(key);
  } catch (error) {
    console.log("Erreur lors de la suppression du last_payment", error);
  }
};

export default {
  storeUser,
  getUser,
  removeUser,
  storeLastPayment,
  getLastPayment,
  removeLastPayment,
};
