export default {
    primary: "#F07D08",
    secondary: "#000000",
    tertiary: "#777777",
    text: "#434040",
    background: "#F4F4F4",
    light: "#EDEDED",
    red: "#FF0000",
    green: "#00AD00",
    white: "#FFFFFF"
}