import React from "react";
import { FontAwesome5 } from "@expo/vector-icons";

import { Platform } from "react-native";
import styled from "styled-components";
import colors from "./colors";
import AppText from "../components/AppText";
import Button from "../components/Button";

// GLOBAL STYLED COMPONENTS
export const Container = styled.View`
  flex: 1;
  background-color: ${colors.background};
`;

export const Section = styled.View`
  padding-top: 20px;
`;

export const SectionBody = styled.View`
  padding-horizontal: 20px;
`;

export const SectionTitle = styled.Text`
  color: ${colors.tertiary};
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 20px;
`;

export const Statistiques = styled.View`
  justify-content: space-between;
  flex-direction: row;
`;

export const StatBox = styled.View`
  background-color: ${colors.white};
  border-radius: 5px;
  elevation: 2;
  width: 47%;
  justify-content: center;
  align-items: center;
  padding: 20px;
`;

export const Devise = styled.Text`
  font-size: 6px;
  align-self: flex-end;
  color: ${colors.text};

  ${(props) =>
    props.primary &&
    `
      color: ${colors.primary};
    `}
`;

export const StatTitle = styled.Text`
  align-self: center;
  font-size: 23px;
  font-weight: bold;
  color: ${colors.text};
`;

export const StatDesc = styled.Text`
  font-size: 10px;
  color: ${colors.tertiary};
`;
export const Content = styled.View`
  padding-vertical: 20px;
`;
export const EmptyContentBox = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.Text`
  color: ${(props) => colors[props.color] || colors.tertiary};
  font-size: ${(props) => props.size || 10}px;
  ${(props) =>
    props.bold &&
    `
      font-weight: bold;
    `}
`;

export const Box = styled.View`
  height: 150px;
  width: 100%;
  background-color: white;
  border-radius: 5px;
  padding: 20px;
  elevation: 3;
  margin-bottom: 20px;
`;

export const shadowStyleIOS = {
  shadowColor: "#000",
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.16,
  shadowRadius: 3,
};

// GLOBAL COMPONENT
export const EmptyContent = ({ message, retry }) => (
  <EmptyContentBox>
    <FontAwesome5 name="meh-blank" size={64} color={colors.tertiary} />
    <AppText style={{ marginTop: 20, color: colors.tertiary }}>
      {message ? message : "Aucun contenu pour le moment"}
    </AppText>
    {retry && <Button title="Actualiser" onPress={retry} />}
  </EmptyContentBox>
);

export default {
  colors,
  text: {
    color: colors.secondary,
    fontSize: 18,
    fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
  },
};
