import currency from "./currency";
// import { Platform } from "react-native";
import * as FileSystem from 'expo-file-system';

export const formatToMoney = (number) => {
  return currency(number, {
    precision: 0,
    symbol: "",
    separator: " ",
  }).format();
};

export const stripString = (string) => {
  return string.replace(/(<([^>]+)>)/gi, "");
};

export const formatUrl = (server, file) => {
  return `${server}/${file}`;
};

export const forfaitLibelle = (string) => {
  const new_str = string.split(";");
  return new_str[0].toUpperCase();
};

export const forfaitPrice = (string) => {
  const new_str = string.split(";");
  const price = new_str[1].toUpperCase();

  return formatToMoney(price);
};

export const convertImageToBase64 = async (uri) => {
  const base64 = await FileSystem.readAsStringAsync(uri, {
    encoding: FileSystem.EncodingType.Base64,
  });
  return `data:image/jpeg;base64,${base64}`;
};
