import React from "react";
import { TouchableWithoutFeedback, View } from "react-native";
import styled from "styled-components";
import colors from "../../config/colors";
import { FontAwesome5 } from "@expo/vector-icons";
import moment from "moment";
import { shadowStyleIOS } from "../../config/styles";

const Box = styled.View`
  background-color: ${colors.white};
  border-radius: 5px;
  elevation: 3;
  flex-direction: column;
  padding: 20px;
  justify-content: space-between;
`;

const Separator = styled.View`
  margin-bottom: 10px;
`;

const Flex = styled.View`
  flex-direction: row;
`;

const Text = styled.Text`
  color: ${(props) => colors[props.color] || colors.tertiary};
  font-size: ${(props) => props.size || 10}px;
  ${(props) =>
    props.bold &&
    `
      font-weight: bold;
    `}
`;

const AgItem = ({ titre, type, date, heure, lieu, description, onPress }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Box style={shadowStyleIOS}>
        <Separator>
          <Text color="text" size={12} bold>
            {titre}
          </Text>
          <Text color="primary" size={10}>
            {type}
          </Text>
        </Separator>
        <Separator>
          <Flex>
            <View>
              <Text>
                <FontAwesome5
                  name="calendar-alt"
                  size={15}
                  color={colors.text}
                />
                {"  "}
                {moment(date).format("DD-MM-YYYY")} {heure}
              </Text>
            </View>
            <View style={{ marginLeft: 10 }}>
              <Text>
                <FontAwesome5 name="map-pin" size={15} color={colors.text} />
                {"  "}
                {lieu}
              </Text>
            </View>
          </Flex>
        </Separator>
        <Text>
          {description}
        </Text>
      </Box>
    </TouchableWithoutFeedback>
  );
};

export default AgItem;
