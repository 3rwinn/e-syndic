import React from "react";
import { StyleSheet, ScrollView, View, Alert, FlatList } from "react-native";
import colors from "../../config/colors";
import styled from "styled-components";
import { FontAwesome5 } from "@expo/vector-icons";

import Button from "../Button";
import useSnackAlert from "../../snackbar/useSnackAlert";
import agApi from "../../api/ag";
import useApi from "../../hooks/useApi";
import useAuth from "../../auth/useAuth";
import useData from "../../hooks/useData";
import { Container } from "../../config/styles";

import moment from "moment";
import { SectionTitle, Section } from "../../config/styles";
import { AppContext } from "../../context/AppState";
moment.locale("fr");

const Content = styled.View`
  padding-vertical: 20px;
`;

const Separator = styled.View`
  margin-bottom: 20px;
  ${(props) =>
    props.ph &&
    `
    padding-horizontal: 20px
  `}
`;

const Flex = styled.View`
  flex-direction: row;
  justify-content: ${(props) =>
    props.nospace ? `flex-start` : `space-between`};
  ${(props) =>
    props.border &&
    `
  border-style: solid;
  border-color: ${colors.light};
  border-bottom-width: 1px;
  border-top-width: 1px;
  `}

  padding-horizontal: ${(props) => (props.nospace ? `0px` : `20px`)};
  padding-vertical: ${(props) => (props.nospace ? `0px` : `10px`)};
`;

const Text = styled.Text`
  color: ${(props) => colors[props.color] || colors.tertiary};
  font-size: ${(props) => props.size || 10}px;
  ${(props) =>
    props.bold &&
    `
      font-weight: bold;
    `}
`;

const FirstContent = ({
  titre,
  type,
  date,
  heure,
  lieu,
  description,
  items,
  showVoting,
}) => {
  return (
    <ScrollView style={styles.container}>
      <Content>
        <Separator ph>
          <Text color="text" size={18} bold>
            {titre}
          </Text>
          <Text color="primary">{type}</Text>
        </Separator>
        <Separator>
          <Flex border>
            <View>
              <Text size={12}>
                <FontAwesome5
                  name="calendar-alt"
                  size={15}
                  color={colors.text}
                />
                {"  "}
                {moment(date).format("DD-MM-YYYY")} {heure}
              </Text>
            </View>
            <View>
              <Text>
                <FontAwesome5 name="map-pin" size={15} color={colors.text} />
                {"  "}
                {lieu}
              </Text>
            </View>
          </Flex>
        </Separator>
        <Separator ph>
          <Text size={14}>{description}</Text>
        </Separator>
        {items.data !== null && (
          <>
            <Separator ph>
              <Text size={12} color="text" style={{ marginBottom: 10 }} bold>
                ORDRE DU JOUR
              </Text>

              {items.data.ordres.map((item, index) => (
                <Flex key={index} nospace>
                  <Text color="primary" size={13} bold>
                    {index + 1} -{" "}
                  </Text>
                  <Text color="primary" size={13} bold>
                    {item.libelle}
                  </Text>
                </Flex>
              ))}
            </Separator>

            <Separator ph>
              <Text size={12} color="text" style={{ marginBottom: 10 }} bold>
                PROJETS DE RESOLUTION
              </Text>
              {items.data.projects.map((item, index) => (
                <View key={index}>
                  <Text color="primary" size={13} bold>
                    PROJET DE RESOLUTION {index + 1} -{" "}
                    {items.data.regle[item.regle]}
                  </Text>

                  <Text color="text" size={13} style={{ paddingVertical: 5 }}>
                    {item.project}
                  </Text>
                </View>
              ))}
            </Separator>
          </>
        )}
        <Separator ph>
          <Button title="ACCEDER AU VOTE" onPress={showVoting} />
        </Separator>
      </Content>
    </ScrollView>
  );
};

const SecondContent = ({ votesData, handleVote }) => {
  console.log("voteDatas", votesData);
  return (
    <Container>
      {votesData?.projects !== null && votesData?.projects?.length > 0 ? (
        <FlatList
          style={{ paddingTop: 20 }}
          data={votesData.projects}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item, index }) => (
            <View
              key={index}
              style={{ marginBottom: 20, marginHorizontal: 20 }}
            >
              <View
                style={{
                  backgroundColor: "white",
                  borderRadius: 5,
                  padding: 20,
                  elevation: 3,
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <View>
                    <Text color="primary" size={14} bold>
                      Projet de résolution {index + 1}
                    </Text>
                    <View style={{ width: "80%", marginTop: 5 }}>
                      <Text style={{ fontSize: 12 }}>
                        {item?.ordre?.libelle}
                      </Text>
                    </View>
                    {item.addvote !== 8 && (
                      <Text style={{ marginTop: 10 }} size={14}>
                        Vous avez voter:{" "}
                        <Text size={13} bold>
                          {item.addvote == 1
                            ? "Pour"
                            : item.addvote === 2
                            ? "Contre"
                            : "Abstention"}
                        </Text>
                      </Text>
                    )}
                  </View>
                  <Button title="Voter" onPress={() => handleVote(item.id)} />
                </View>
              </View>
            </View>
          )}
        />
      ) : (
        <Section>
          <SectionTitle style={{ textAlign: "center", fontSize: 16 }}>
            Les votes ne sont pas ouverts.
          </SectionTitle>
        </Section>
      )}
    </Container>
  );
};

const AgDetail = ({ id, titre, type, date, heure, lieu, description }) => {
  const snack = useSnackAlert();

  const { user } = useAuth();
  const { selectedProgram } = React.useContext(AppContext);
  const loadAgItemsApi = useApi(agApi.loadUserAgItems);
  const loadAgVotesApi = useApi(agApi.loadUserAgVote);
  const addVoteApi = useApi(agApi.addVote);

  React.useEffect(() => {
    loadAgItemsApi.request(user.id, id, selectedProgram);
    loadAgVotesApi.request(user.id, id, selectedProgram);
  }, []);

  const items = useData(loadAgItemsApi.data, loadAgItemsApi.data.datas);
  let projetVote = useData(loadAgVotesApi.data, loadAgVotesApi.data.datas);

  const [isReloading, setReloading] = React.useState(false);
  React.useEffect(() => {
    if (isReloading) {
      loadAgVotesApi.request(user.id, id, selectedProgram);
    }
  }, [isReloading]);

  const [votesData, setVotesData] = React.useState(null);
  React.useEffect(() => {
    if (!isReloading) {
      projetVote.data && setVotesData(projetVote.data);
    } else if (isReloading && loadAgVotesApi.data) {
      setVotesData(loadAgVotesApi.data.datas);
      setReloading(false);
    }
  }, [projetVote, loadAgVotesApi.data]);

  const handleVote = (pjt_id) => {
    Alert.alert(
      "Vote",
      "Merci de choisir une des options ci-dessous",
      [
        {
          text: "Pour",
          onPress: () => {
            addVoteApi.request(user.id, pjt_id, id, "1", selectedProgram);
            snack.showAlert("Vote enregistré avec succès", "success");
            setReloading(true);
          },
        },
        {
          text: "Contre",
          onPress: () => {
            addVoteApi.request(user.id, pjt_id, id, "2", selectedProgram);
            snack.showAlert("Vote enregistré avec succès", "success");
            setReloading(true);
          },
          // style: "cancel",
        },
        {
          text: "Abstention",
          onPress: () => {
            addVoteApi.request(user.id, pjt_id, id, "0", selectedProgram);
            snack.showAlert("Vote enregistré avec succès", "success");
            setReloading(true);
          },
        },
      ],
      { cancelable: false }
    );
  };

  const [modalContent, setModalContent] = React.useState("ag");
  return (
    <>
      {modalContent === "ag" && items.data !== null ? (
        <FirstContent
          titre={titre}
          type={type}
          date={date}
          heure={heure}
          lieu={lieu}
          description={description}
          items={items}
          showVoting={() => setModalContent("vote")}
        />
      ) : modalContent === "vote" ? (
        <SecondContent votesData={votesData} handleVote={handleVote} />
      ) : null}
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default AgDetail;
