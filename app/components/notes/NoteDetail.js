import React from "react";
import { StyleSheet, ScrollView, useWindowDimensions } from "react-native";
import styled from "styled-components";
import { Feather } from "@expo/vector-icons";
import HTML from "react-native-render-html";

import colors from "../../config/colors";
import AppText from "../AppText";
import { Section, SectionBody } from "../../config/styles";

import moment from "moment";
import "moment/locale/fr";
moment.locale("fr");

const NoteUserInfo = styled.View`
  flex-direction: row;
`;
const UserPic = styled.Image`
  height: 40px;
  width: 40px;
  border-radius: 50px;
  background-color: ${colors.light};
`;
const UserInfo = styled.View`
  margin-left: 10px;
`;
const Text = styled(AppText)`
  color: ${(props) => colors[props.color]};
  font-size: ${(props) => props.size};
  ${(props) =>
    props.bold &&
    `
        font-weight: bold;
    `}
`;

const NoteDetailBox = styled.View`
  margin-top: 10px;
  ${(props) =>
    props.btm &&
    `
  margin-bottom: 10px
  `}
`;

const NoteDetail = ({ objet, picture, text, date }) => {
  const { width } = useWindowDimensions();

  return (
    <ScrollView style={styles.container}>
      <Section>
        <SectionBody>
          <NoteUserInfo>
            <UserPic source={{ uri: picture }} />
            <UserInfo>
              <Text color="tertiary" size="14px" bold>
                {objet}
              </Text>
              <Text color="text" size="10px">
                Concierge
              </Text>
            </UserInfo>
          </NoteUserInfo>
          <NoteDetailBox>
            <Text size="12px" color="tertiary">
              <Feather name="clock" size={12} color={colors.tertiary} />{" "}
              {moment(date).fromNow()}
            </Text>
          </NoteDetailBox>
          <NoteDetailBox btm>
            {/* <Text size="12px" color="text">
              {text}
            </Text> */}
            <HTML contentWidth={width} source={{ html: text }} />
          </NoteDetailBox>
        </SectionBody>
      </Section>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default NoteDetail;
