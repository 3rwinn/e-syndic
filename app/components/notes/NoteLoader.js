import React from "react";
import { useWindowDimensions } from "react-native";
import ContentLoader, { Rect, Circle, Path } from "react-content-loader/native";
import styled from "styled-components";
import { Section, SectionBody } from "../../config/styles";
import colors from "../../config/colors";

const Box = styled.View`
  background-color: ${colors.white};
  border-radius: 5px;
  padding-horizontal: 20px;
  padding-vertical: 10px;
  elevation: 3;
  margin-bottom: 20px;
  flex-direction: column;
  justify-content: space-between;
`;

const Loading = (props) => {
  const window = useWindowDimensions();
  const mainWidth = window.width;
  return (
    <ContentLoader
      speed={1}
      width={mainWidth}
      height={134}
      viewBox={`0 0 ${mainWidth} 134`}
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <Circle cx="30" cy="30" r="30" />
      <Rect x="78" y="8" rx="3" ry="3" width={140} height="8" />
      <Rect x="78" y="25" rx="3" ry="3" width={88} height="8" />
      <Rect x="0" y="79" rx="3" ry="3" width={mainWidth - 89} height="8" />
      <Rect x="0" y="99" rx="3" ry="3" width={mainWidth - 89} height="8" />
      <Rect x="0" y="119" rx="3" ry="3" width={mainWidth - 109} height="8" />

      {/* <Rect x="98" y="42" rx="3" ry="3" width="319" height="8" />
    <Rect x="3" y="5" rx="5" ry="5" width="70" height="70" /> */}
    </ContentLoader>
  );
};

const NoteLoader = () => {
  return (
    <Section>
      <SectionBody>
        <Box>
          <Loading />
        </Box>
        <Box>
          <Loading />
        </Box>
        <Box>
          <Loading />
        </Box>
      </SectionBody>
    </Section>
  );
};

export default NoteLoader;
