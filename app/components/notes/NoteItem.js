import React from "react";
import { TouchableWithoutFeedback } from "react-native";
import styled from "styled-components";
import { Feather } from "@expo/vector-icons";

import colors from "../../config/colors";
import AppText from "../AppText";

import moment from "moment";
import { shadowStyleIOS } from "../../config/styles";
moment.locale("fr");

const NoteBox = styled.View`
background-color: ${colors.white};
width: 100%;
border-radius: 5px;
padding: 20px;
elevation: 3;
overflow: hidden;
`;

const NoteUserInfo = styled.View`
  flex-direction: row;
`;
const UserPic = styled.Image`
  height: 40px;
  width: 40px;
  border-radius: 50px;
  background-color: ${colors.light};
`;
const UserInfo = styled.View`
  margin-left: 10px;
`;
const Text = styled(AppText)`
  color: ${(props) => colors[props.color]};
  font-size: ${(props) => props.size};
  ${(props) =>
    props.bold &&
    `
        font-weight: bold;
    `}
`;

const NoteDetail = styled.View`
  margin-vertical: 10px;
`;

const NoteItem = ({ objet, picture, text, date, onPress }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <NoteBox style={shadowStyleIOS}>
        <NoteUserInfo>
          <UserPic source={{ uri: picture }} />
          <UserInfo>
            <Text color="tertiary" size="14px" bold>
              {objet}
            </Text>
            <Text color="text" size="10px">
              Concierge
            </Text>
          </UserInfo>
        </NoteUserInfo>
        <NoteDetail>
          <Text size="12px" color="text" numberOfLines={5}>
            {text}
          </Text>
        </NoteDetail>
        <Text size="12px" color="tertiary">
          <Feather name="clock" size={12} color={colors.tertiary} /> {moment(date).fromNow()}
        </Text>
      </NoteBox>
    </TouchableWithoutFeedback>
  );
};

export default NoteItem;
