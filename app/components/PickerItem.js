import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";
import { FontAwesome } from "@expo/vector-icons";

import Text from "./AppText";
import colors from "../config/colors";

function PickerItem({ item, onPress, itemColor }) {
  return (
    <TouchableOpacity
      style={{ flexDirection: "row", alignItems: "center", padding: 20 }}
      onPress={onPress}
    >
      <FontAwesome name="chevron-right" size={25} color={colors[itemColor]} />
      <Text style={[styles.text, { color: itemColor ? colors[itemColor] : '#000' }]}>
        {item.label}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  text: {
    marginLeft: 20,
    fontWeight: "bold",
    fontSize: 16,
  },
});

export default PickerItem;
