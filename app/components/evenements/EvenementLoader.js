import React from "react";
import { useWindowDimensions } from "react-native";
import ContentLoader, { Rect, Circle, Path } from "react-content-loader/native";
import { Section, SectionBody } from "../../config/styles";
import styled from "styled-components";

const Box = styled.View`
  height: 180px;
  width: 100%;
  background-color: white;
  border-radius: 5px;
  padding: 20px;
  elevation: 3;
  margin-bottom: 20px;
`;

const Loading = (props) => {
  const window = useWindowDimensions();
  const mainWidth = window.width;
  return (
    <ContentLoader
      speed={1}
      width={mainWidth}
      height={184}
      viewBox={`0 10 ${mainWidth} 184`}
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <Rect x="0" y="8" rx="3" ry="3" width={mainWidth - 99} height="8" />
      <Rect x="0" y="22" rx="3" ry="3" width={mainWidth - 140} height="8" />
      <Rect x="0" y="56" rx="3" ry="3" width={mainWidth - 99} height="8" />
      <Rect x="0" y="72" rx="3" ry="3" width={mainWidth - 99} height="8" />
      <Rect x="0" y="88" rx="3" ry="3" width={mainWidth - 99} height="8" />

      <Circle cx="20" cy="130" r="20" />
      <Rect x="50" y="120" rx="3" ry="3" width="178" height="8" />
      <Rect x="50" y="136" rx="3" ry="3" width="148" height="8" />
    </ContentLoader>
  );
};

const EvenementLoader = () => {
  return (
    <Section>
      <SectionBody>
        <Box>
          <Loading />
        </Box>
        <Box>
          <Loading />
        </Box>
        <Box>
          <Loading />
        </Box>
      </SectionBody>
    </Section>
  );
};

export default EvenementLoader;
