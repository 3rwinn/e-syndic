import React from "react";
import { StyleSheet, View, useWindowDimensions } from "react-native";
import styled from "styled-components";
import { Feather, MaterialIcons } from "@expo/vector-icons";
import HTML from "react-native-render-html";

import colors from "../../config/colors";
import AppText from "../AppText";

const EvenementContent = styled.ScrollView`
  padding: 20px;
`;

const Row = styled.View`
  flex-direction: row;
  ${(props) =>
    props.sb &&
    `
    justify-content: space-between
  `}
`;

const Separator = styled.View`
  margin-bottom: 10px;
`;

const Text = styled(AppText)`
  color: ${(props) => (props.color ? colors[props.color] : colors.text)};
  font-size: ${(props) => (props.size ? props.size : "12px")};
  margin-bottom: ${(props) => (props.mb ? props.mb : "0px")};
    ${(props) => props.bold && `font-weight: bold`};
`;

const Media = styled.Image`
  width: 100%;
  height: 226px;
  background-color: ${colors.light};
  border-radius: 5px;
  overflow: hidden;
`;

const Line = styled.View`
  width: 100%;
  height: 0.7px;
  background-color: ${colors.light};
`;

const UserPic = styled.Image`
  height: 40px;
  width: 40px;
  border-radius: 50px;
  background-color: ${colors.light};
`;
const UserInfo = styled.View`
  margin-left: 10px;
`;

const EvenementDetail = ({
  titre,
  picture,
  datedebut,
  datefin,
  bruit,
  text,
  author,
  image,
}) => {
  const { width } = useWindowDimensions();
  return (
    <View style={styles.container}>
      <EvenementContent>
        <Separator>
          <Text size="26px" mb="5px" bold>
            {titre}
          </Text>
          <Row sb>
            <Text color="tertiary">
              <Feather name="clock" size={12} color={colors.tertiary} /> {datedebut} - {datefin}
            </Text>
            {bruit === 1 && (
              <Text color="tertiary">
                <MaterialIcons
                  name="hearing"
                  size={12}
                  color={colors.tertiary}
                />{" "}
                Bruyant
              </Text>
            )}
          </Row>
        </Separator>
        {image && (
          <Separator>
            <Media source={{ uri: image }} />
          </Separator>
        )}
        {/* <Text>{text}</Text> */}
        <HTML contentWidth={width} source={{ html: text }} />
      </EvenementContent>
      {author && (
        <>
          <Line />
          <EvenementContent>
            <Row>
              <UserPic source={{ uri: picture }} />
              <UserInfo>
                <Text color="tertiary" size="14px" bold>
                  {author}
                </Text>
                <Text color="text" size="10px">
                  Riverrain
                </Text>
              </UserInfo>
            </Row>
          </EvenementContent>
        </>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default EvenementDetail;
