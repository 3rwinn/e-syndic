import React, { useContext, useState } from "react";
import { StyleSheet, ScrollView } from "react-native";
import {
  Form,
  FormDatetimeField,
  FormField,
  FormRadioField,
  FormSingleImagePicker,
  SubmitButton,
} from "../../components/forms";
import * as Yup from "yup";
import styled from "styled-components";

import colors from "../../config/colors";
import { Section, SectionBody } from "../../config/styles";

import evenementApi from "../../api/evenement";
import useAuth from "../../auth/useAuth";
import useApi from "../../hooks/useApi";
import useSnackAlert from "../../snackbar/useSnackAlert";
import { AppContext } from "../../context/AppState";
import { convertImageToBase64 } from "../../utils/helpers";

const validationSchema = Yup.object().shape({
  titre: Yup.string().required("Ce champ est requis"),
  description: Yup.string().required("Ce champ est requis"),
  // photo: Yup.mixed().required("Merci de choisir une photo"),
  date_debut: Yup.string().required("Ce champ est requis"),
  date_fin: Yup.string().required("Ce champ est requis"),
});

const AddEvenement = ({ onClose, changeVisibility }) => {
  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  const addEvenementApi = useApi(evenementApi.addEvenement);
  const [loading, setLoading] = useState(false);
  const snack = useSnackAlert();
  const handleSubmit = async (values, { resetForm }) => {
    setLoading(true);
    const newevent = {
      user_id: user.id,
      title: values.titre,
      desc: values.description,
      photo: await convertImageToBase64(values.photo),
      ddebut: values.date_debut,
      dfin: values.date_fin,
      bruit: values.bruit,
      program: selectedProgram,
    };

    const result = await addEvenementApi.request(newevent);
    // console.log("momo", result);
    console.log(result.data);
    setLoading(false);
    if (result?.data?.message === "Evenement create") {
      snack.showAlert(
        "L'événement a bien été créé, il est en attente de validation",
        "success"
      );
      // alert("L'événement a bien été créé, il est en attente de validation");
    } else {
      snack.showAlert(
        "Une erreur est survenue, merci ré-essayer plus tard",
        "error"
      );
    }
    resetForm();
    onClose();
  };

  return (
    <ScrollView style={styles.container}>
      <Section>
        <SectionBody>
          <Form
            initialValues={{
              titre: "",
              description: "",
              photo: null,
              date_debut: "",
              date_fin: "",
              bruit: false,
            }}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <FormField name="titre" placeholder="Titre" />
            <FormField
              name="description"
              placeholder="Description"
              numberOfLines={3}
              multiline
            />
            <FormSingleImagePicker
              name="photo"
              placeholder="Photo d'illustration"
              oEvent={changeVisibility}
            />
            <FormRadioField name="bruit" placeholder="Bruyant" />
            <FormDatetimeField name="date_debut" placeholder="Date de début" />
            <FormDatetimeField name="date_fin" placeholder="Date de fin" />

            <SubmitButton title="Enregistrer" loading={loading} />
          </Form>
        </SectionBody>
      </Section>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default AddEvenement;
