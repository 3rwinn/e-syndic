import React from "react";
import { TouchableWithoutFeedback } from "react-native";
import styled from "styled-components";
import colors from "../../config/colors";
import { stripString } from "../../utils/helpers";
// import HTML from "react-native-render-html";

import moment from "moment";
import { shadowStyleIOS } from "../../config/styles";
moment.locale("fr");

const MessageBox = styled.View`
  background-color: ${colors.white};
  border-radius: 5px;
  elevation: 3;
  flex-direction: row;
  padding: 20px;
`;

const MessageAvatar = styled.Image`
  height: 60px;
  width: 60px;
  border-radius: 50px;
  background-color: ${colors.light};
`;

const MessageContentBox = styled.View`
  width: 85%;
  padding-horizontal: 20px;
`;

const TopContent = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

const Text = styled.Text`
  color: ${(props) => colors[props.color] || colors.tertiary};
  font-size: ${(props) => props.size || 10}px;
  ${(props) =>
    props.bold &&
    `
      font-weight: bold;
    `}
`;

const MessageItem = ({
  destinataire,
  picture,
  date,
  objet,
  contenu,
  onPress,
  mode,
}) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <MessageBox
        style={shadowStyleIOS}
      >
        <MessageAvatar source={{ uri: picture }} />
        <MessageContentBox>
          <TopContent>
            <Text color="primary" bold>
              {mode === "out" ? "À: " : "DE: "}
              {destinataire}
            </Text>
            <Text>{moment(date).fromNow()}</Text>
          </TopContent>
          <Text numberOfLines={1} color="text" size={12} bold>
            {objet}
          </Text>
          <Text numberOfLines={2}>{stripString(contenu)}</Text>
          {/* <HTML html={contenu}  /> */}
        </MessageContentBox>
      </MessageBox>
    </TouchableWithoutFeedback>
  );
};

export default MessageItem;
