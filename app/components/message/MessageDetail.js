import React from "react";
import {
  StyleSheet,
  ScrollView,
  FlatList,
  useWindowDimensions,
} from "react-native";
import styled from "styled-components";
import { Feather, FontAwesome5 } from "@expo/vector-icons";
import HTML from "react-native-render-html";

import { Section, SectionBody, SectionTitle } from "../../config/styles";
import colors from "../../config/colors";
import AppText from "../AppText";
import moment from "moment";
import "moment/locale/fr";
moment.locale("fr");

const Separator = styled.View`
  margin-bottom: 10px;
`;

const Row = styled.View`
  flex-direction: row;
  width: 100%;
  ${(props) =>
    props.sb &&
    `
    justify-content: space-between
  `}
`;
// const UserPic = styled.View`
const UserPic = styled.Image`
  height: 40px;
  width: 40px;
  border-radius: 50px;
  background-color: ${colors.light};
`;
const UserInfo = styled.View`
  margin-left: 10px;
`;
const Text = styled(AppText)`
  color: ${(props) => (props.color ? colors[props.color] : colors.text)};
  font-size: ${(props) => (props.size ? props.size : "12px")};
  margin-bottom: ${(props) => (props.mb ? props.mb : "0px")};
  ${(props) => props.bold && `font-weight: bold`};
`;

const DocumentItem = styled.TouchableOpacity`
  align-items: center;
  border: 1px solid ${colors.tertiary};
  border-radius: 5px;
  flex-direction: row;
  padding: 10px;
  margin-bottom: 10px;
  width: 100%;
`;
const DocumentContent = styled.View`
  flex-direction: column;
  margin-left: 10px;
`;

const MessageDetail = ({
  destinataire,
  picture,
  objet,
  date,
  contenu,
  documents,
  mode,
}) => {
  const { width } = useWindowDimensions();
  return (
    <ScrollView style={styles.container}>
      <Section>
        <SectionBody>
          <Separator>
            <Row>
              <UserPic source={{ uri: picture }} />
              <UserInfo>
                <Text color="tertiary" size="14px" bold>
                  {mode === "out" ? "À: " : "DE: "}
                  {destinataire}
                </Text>
                <Text color="text" size="10px">
                  <Feather name="clock" size={10} color={colors.text} />{" "}
                  {moment(date).fromNow()}
                </Text>
              </UserInfo>
            </Row>
          </Separator>
          <Separator>
            <Text size="12px" color="tertiary" bold>
              {objet}
            </Text>
          </Separator>
          <Separator>
            {/* <Text>{contenu}</Text> */}
            <HTML contentWidth={width} source={{ html: contenu }} />
          </Separator>
          {documents && (
            <Separator>
              <SectionTitle>Pièce jointes</SectionTitle>
              <FlatList
                keyExtractor={(item) => item.id.toString()}
                data={documents}
                renderItem={({ item }) => (
                  <DocumentItem key={item.id}>
                    <FontAwesome5 name="file-pdf" size={30} color="red" />
                    <DocumentContent>
                      <Text bold>{item.label}</Text>
                      {/* <Text>31/12/2020</Text> */}
                    </DocumentContent>
                  </DocumentItem>
                )}
              />
            </Separator>
          )}
        </SectionBody>
      </Section>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default MessageDetail;
