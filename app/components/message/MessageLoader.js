import React from "react";
import { useWindowDimensions } from "react-native";
import ContentLoader, { Rect, Circle, Path } from "react-content-loader/native";
import { Section, SectionBody } from "../../config/styles";
import styled from "styled-components";
const Box = styled.View`
  height: 126px;
  width: 100%;
  background-color: white;
  border-radius: 5px;
  padding-horizontal: 20px;
  padding-vertical: 10px;
  elevation: 3;
  margin-bottom: 20px;
`;

const Loading = (props) => {
  const window = useWindowDimensions();
  const mainWidth = window.width;
  return (
    <ContentLoader
      speed={1}
      width={mainWidth}
      height={134}
      viewBox={`0 0 ${mainWidth} 134`}
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <Rect x="98" y="8" rx="3" ry="3" width="88" height="8" />
      <Circle cx="40" cy="40" r="40" />
      <Rect x="98" y="25" rx="3" ry="3" width={mainWidth - 180} height="8" />
      <Rect x="98" y="42" rx="3" ry="3" width={mainWidth - 180} height="8" />
      <Rect x="98" y="59" rx="3" ry="3" width={mainWidth - 180} height="8" />
    </ContentLoader>
  );
};

const MessageLoader = (props) => (
  <Section>
    <SectionBody>
      <Box>
        <Loading />
      </Box>
      <Box>
        <Loading />
      </Box>
      <Box>
        <Loading />
      </Box>
    </SectionBody>
  </Section>
);

export default MessageLoader;
