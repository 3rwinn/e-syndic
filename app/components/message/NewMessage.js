import React, { useContext, useEffect } from "react";
import { StyleSheet, Modal, Platform } from "react-native";
import styled from "styled-components";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import AppText from "../AppText";
import colors from "../../config/colors";
// import { SectionTitle } from "../../config/styles";
import messageApi from "../../api/message";

import { Form, FormField, SubmitButton } from "../../components/forms";
import * as Yup from "yup";
import useApi from "../../hooks/useApi";
import useAuth from "../../auth/useAuth";
import useSnackAlert from "../../snackbar/useSnackAlert";
import { AppContext } from "../../context/AppState";
import message from "../../api/message";

const NewMessageBox = styled.TouchableOpacity`
  align-items: center;
  background-color: ${colors.primary};
  border-radius: 50px;
  bottom: 20px;
  elevation: 3;
  height: 50px;
  justify-content: center;
  position: absolute;
  right: 20px;
  width: 50px;
`;
const ModalHeader = styled.View`
  align-items: center;
  background-color: ${colors.primary};
  flex-direction: row;
  padding-top: ${Platform.OS === "ios" ? "50px" : "20px"};
  padding-bottom: 20px;
  padding-horizontal: 20px;
  width: 100%;
  elevation: 20;
`;
const Text = styled(AppText)`
  color: ${(props) => (props.color ? props.color : "white")};
  font-weight: bold;
`;
const Icon = styled.TouchableOpacity`
  margin-right: 20px;
`;
const ModalBody = styled.View`
  padding: 20px;
`;

const validationSchema = Yup.object().shape({
  // destinataire: Yup.string().required("Ce champ est requis"),
  objet: Yup.string().required("Ce champ est requis"),
  message: Yup.string().required("Ce champ est requis"),
});

const NewMessage = () => {
  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  const [modalVisible, setModalVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const snack = useSnackAlert();
  const sendMessageApi = useApi(messageApi.sendMessage);

  const handleSubmit = async (values, { resetForm }) => {
    const newmessage = {
      user_id: user.id,
      objet: values.objet,
      contenu: values.message,
      program: selectedProgram,
    };
    // const result = await sendMessageApi.request(newmessage);
    setLoading(true);

    const result = await message.sendMessage(newmessage)

    console.log("result", result);
    setLoading(false);
    if (result.data.messages === "Message send") {
      // alert("Message envoyé");
      snack.showAlert("Votre message a bien été envoyé", "success");
    } else {
      // alert("TESTTTTT")
      // console.log("result", result.data);
      snack.showAlert(
        "Une erreur est survenue, merci de ré-essayer plus tard",
        "error"
      );
      // alert("TEST2")
    }

    // resetForm();
    setModalVisible(false);
  };

  return (
    <>
      <NewMessageBox onPress={() => setModalVisible(true)}>
        <MaterialCommunityIcons name="pencil" size={24} color={colors.white} />
      </NewMessageBox>
      <Modal visible={modalVisible} animationType="slide">
        <ModalHeader>
          <Icon onPress={() => setModalVisible(false)}>
            <MaterialCommunityIcons
              name="close"
              size={30}
              color={colors.white}
            />
          </Icon>
          <Text>Nouveau message</Text>
        </ModalHeader>
        <ModalBody>
          {/* <SectionTitle>Nouveau message</SectionTitle> */}
          <Form
            initialValues={{
              // destinataire: "",
              objet: "",
              message: "",
            }}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            {/* <FormField name="destinataire" placeholder="Destinataire(s)" /> */}
            <FormField name="objet" placeholder="Objet" />
            <FormField
              name="message"
              multiline
              numberOfLines={3}
              placeholder="Message"
            />
            <SubmitButton title="ENVOYER" loading={loading} />
          </Form>
        </ModalBody>
      </Modal>
    </>
  );
};
const styles = StyleSheet.create({
  container: {},
});
export default NewMessage;
