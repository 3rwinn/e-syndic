import React from "react";
import { Modal, Platform } from "react-native";
import styled from "styled-components";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Constants from "expo-constants";

import colors from "../config/colors";
import AppText from "./AppText";
import SnackAlert from "../snackbar/SnackAlert";
import Screen from "./Screen";

const ModalHeader = styled.View`
  align-items: center;
  background-color: ${colors.primary};
  flex-direction: row;
  padding-top: ${Platform.OS === "ios" ? "50px" : "20px"};
  padding-bottom: 20px;
  padding-horizontal: 20px;
  width: 100%;
  elevation: 20;
`;
const Text = styled(AppText)`
  color: ${(props) => (props.color ? props.color : "white")};
  font-weight: bold;
`;
const Icon = styled.TouchableOpacity`
  margin-right: 20px;
`;
const ModalBody = styled.View`
  flex: 1;
  background-color: ${colors.background};
`;

const AppModal = ({ visible, toggleModal, title, content, reset }) => {
  return (
    <Modal visible={visible} animationType="slide">
      <ModalHeader>
        <Icon
          onPress={() => {
            toggleModal();
            reset && reset();
          }}
        >
          <MaterialCommunityIcons name="close" size={30} color={colors.white} />
        </Icon>
        <Text>{title}</Text>
      </ModalHeader>
      <ModalBody>
        {content}

        <SnackAlert />
      </ModalBody>
    </Modal>
  );
};

export default AppModal;
