import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Feather } from "@expo/vector-icons";
import colors from "../config/colors";
import AppText from "../components/AppText";

const TopBar = ({
  title,
  navigation,
  specialBtn,
  showDrawer = true,
  isFakeModal = false,
}) => {
  return (
    <View style={styles.topBar}>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        {showDrawer && (
          <>
            {!isFakeModal ? (
              <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
                <Feather name="menu" color={colors.white} size={30} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <Feather name="x" color={colors.white} size={30} />
              </TouchableOpacity>
            )}
          </>
        )}
        <AppText style={[styles.title, { marginLeft: showDrawer ? 20 : 0 }]}>
          {title}
        </AppText>
      </View>
      <View>{specialBtn}</View>
    </View>
  );
};
const styles = StyleSheet.create({
  topBar: {
    width: "100%",
    backgroundColor: colors.primary,
    padding: 20,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  title: {
    color: colors.white,
    fontWeight: "bold",
  },
});
export default TopBar;
