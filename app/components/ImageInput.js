import React, { useEffect } from "react";
import {
  View,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  Alert,
} from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import * as ImagePicker from "expo-image-picker";

import colors from "../config/colors";

function ImageInput({ imageUri, onChangeImage, bordeRadius = 5 }) {
  // useEffect(() => {
  //   requestPermission();
 
  // }, []);

  // const requestPermission = async () => {
  //   const { granted } = await ImagePicker.requestMediaLibraryPermissionsAsync();
  //   // const { status } = await ImagePicker.requestCameraPermissionsAsync();
  //   // if (!status !== "granted")
  //   //   alert(
  //   //     "Vous devez accepter la permission pour accéder a votre galerie d'images."
  //   //   );
  //   if (!granted)
  //     alert(
  //       "E-syndic a besoin de votre permission pour accéder à la galerie pour vous permettre de choisir une photo."
  //     );
  // };

  const handlePress = () => {
    if (!imageUri) selectImage();
    else
      Alert.alert(
        "Supprimer",
        "Êtes-vous sure de vouloir supprimer cette image?",
        [{ text: "Oui", onPress: () => onChangeImage(null) }, { text: "Non" }]
      );
  };

  const selectImage = async () => {
    try {
      // const result = await ImagePicker.launchCameraAsync();
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 0.5,
      });
      // if (!result.cancelled) onChangeImage(result.uri);
      if (!result.canceled) onChangeImage(result.assets[0].uri);
    } catch (error) {
      console.log("Error reading an image", error);
    }
  };

  return (
    <TouchableWithoutFeedback onPress={handlePress}>
      <View style={[styles.container, { borderRadius: bordeRadius }]}>
        {!imageUri && (
          <MaterialCommunityIcons
            color={colors.medium}
            name="camera"
            size={40}
          />
        )}
        {imageUri && <Image source={{ uri: imageUri }} style={styles.image} />}
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: colors.light,
    // borderRadius: 15,
    height: 100,
    justifyContent: "center",
    marginVertical: 10,
    overflow: "hidden",
    width: 100,
  },
  image: {
    height: "100%",
    width: "100%",
  },
});

export default ImageInput;
