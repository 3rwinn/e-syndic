import React from "react";
import { StyleSheet, View, Image } from "react-native";

const ImageView = ({ image }) => {
  return (
    <View style={styles.container}>
      {image && (
        <Image
          source={{ uri: image }}
          resizeMode="contain"
          style={styles.image}
        />
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000",
  },
  image: {
    width: "100%",
    height: "100%",
  },
});
export default ImageView;
