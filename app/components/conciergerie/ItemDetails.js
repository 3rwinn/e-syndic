import React from "react";
import { StyleSheet, ScrollView, View, useWindowDimensions } from "react-native";
import { Content, Text } from "../../config/styles";
import Button from "../Button";
import styled from "styled-components";
import colors from "../../config/colors";
import useModal from "../../hooks/useModal";
import AppModal from "../Modal";
import ItemCommand from "./ItemCommand";
import HTML from "react-native-render-html";
import { forfaitLibelle, forfaitPrice } from "../../utils/helpers";

const Separator = styled.View`
  margin-bottom: 20px;
  ${(props) =>
    props.ph &&
    `
    padding-horizontal: 20px
  `}
`;
const Flex = styled.View`
  flex-direction: row;
  justify-content: ${(props) =>
    props.nospace ? `flex-start` : `space-between`};
  ${(props) =>
    props.border &&
    `
  border-style: solid;
  border-color: ${colors.light};
  border-bottom-width: 1px;
  border-top-width: 1px;
  `}

  padding-horizontal: ${(props) => (props.nospace ? `0px` : `20px`)};
  padding-vertical: ${(props) => (props.nospace ? `0px` : `10px`)};
`;

const ItemDetails = ({
  product_id,
  titre,
  category,
  description,
  prix,
  mode,
  forfaits,
}) => {
  const commandModal = useModal();
  const { width } = useWindowDimensions();

  return (
    <ScrollView style={styles.container}>
      <Content>
        <Separator ph>
          <Text size={18} bold>
            {titre}
          </Text>
          <Text color="primary">{category}</Text>
        </Separator>
        <Separator>
          <Flex border>
            {mode !== "service" ? (
              <Text size={16} color="primary" bold>
                {prix} FCFA
              </Text>
            ) : (
              <View style={{ flexDirection: "column" }}>
                {forfaits !== null && forfaits.length > 0 ? (
                  forfaits.map((forfait, index) => (
                    <Text key={index} size={16} color="primary" bold>
                      {forfaitLibelle(forfait)} : {forfaitPrice(forfait)} FCFA
                      {index !== forfaits.length - 1 && ";"}
                    </Text>
                  ))
                ) : (
                  <Text size={16} color="primary" bold>
                    NOUS CONSULTER
                  </Text>
                )}
              </View>
            )}
          </Flex>
        </Separator>
        <Separator ph>
          {/* <Text size={14}>{description}</Text> */}
          {/* <HTML html={description} /> */}
          <HTML contentWidth={width} source={{ html: description }} />
          
        </Separator>
        <Separator ph>
          <Button
            title="COMMANDER"
            onPress={() =>
              commandModal.toggleModal(
                "Commander",
                <ItemCommand
                  product_id={product_id}
                  onClose={() => commandModal.close()}
                />
              )
            }
          />
        </Separator>
      </Content>
      <AppModal
        visible={commandModal.modalVisible}
        title={commandModal.modalTitle}
        content={commandModal.modalContent}
        toggleModal={commandModal.toggleModal}
        close={commandModal.close}
      />
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default ItemDetails;
