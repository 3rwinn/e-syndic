import React from "react";
import { View, useWindowDimensions } from "react-native";
import ContentLoader, { Rect, Circle, Path } from "react-content-loader/native";
import styled from "styled-components";
import colors from "../../config/colors";

const Box = styled.View`
  height: 126px;
  width: 100%;
  background-color: white;
  padding-horizontal: 20px;
  padding-vertical: 10px;
  border-bottom-color: ${colors.light};
  border-bottom-width: 1px;
`;

const Loading = (props) => {
  const window = useWindowDimensions();
  const mainWidth = window.width;
  return (
    <ContentLoader
      speed={1}
      width={mainWidth}
      height={134}
      viewBox={`0 0 ${mainWidth} 124`}
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <Rect x="98" y="8" rx="3" ry="3" width="88" height="8" />
      {/* <Circle cx="40" cy="40" r="40" /> */}
      <Rect x="98" y="25" rx="3" ry="3" width={mainWidth - 149} height="8" />
      <Rect x="98" y="42" rx="3" ry="3" width={mainWidth - 149} height="8" />
      <Rect x="98" y="59" rx="3" ry="3" width={mainWidth - 149} height="8" />
      <Rect x="3" y="5" rx="5" ry="5" width="70" height="70" />
    </ContentLoader>
  );
};

const ItemLoader = () => {
  return (
    <View style={{ backgroundColor: "white", flex: 1 }}>
      <Box>
        <Loading />
      </Box>
      <Box>
        <Loading />
      </Box>
      <Box>
        <Loading />
      </Box>
      <Box>
        <Loading />
      </Box>
      <Box>
        <Loading />
      </Box>
    </View>
  );
};

export default ItemLoader;
