import React from "react";
import { StyleSheet, ScrollView } from "react-native";
import { Content } from "../../config/styles";
import * as Yup from "yup";
import { Form, FormField, SubmitButton } from "../../components/forms";
import colors from "../../config/colors";
import useAuth from "../../auth/useAuth";
import useApi from "../../hooks/useApi";
import conciergerie from "../../api/conciergerie";
import useSnackAlert from "../../snackbar/useSnackAlert";
import { AppContext } from "../../context/AppState";

const validationSchema = Yup.object().shape({
  message: Yup.string().required("Ce champ est requis"),
});

const ItemCommand = ({ product_id, onClose }) => {
  const { user } = useAuth();
  const { selectedProgram } = React.useContext(AppContext);
  const askProduct = useApi(conciergerie.askProduct);
  const snack = useSnackAlert();
  const [loading, setLoading] = React.useState(false);
  const handleSubmit = async ({ message, product }) => {
    setLoading(true);
    const result = await askProduct.request(
      user.id,
      product,
      message,
      selectedProgram
    );
    setLoading(false);
    if (result.data) {
      // console.log("result", result)
      snack.showAlert(result.data.message);
      // alert(result.data.message)
      onClose();
    }
  };

  return (
    <ScrollView style={styles.container}>
      <Content style={{ paddingHorizontal: 20 }}>
        <Form
          initialValues={{
            product: product_id,
            nom: user.name + " " + user.prenoms,
            telephone: user.contact,
            message: "",
          }}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
        >
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            name="nom"
            placeholder="Nom complet"
            editable={false}
          />
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="phone-pad"
            name="telephone"
            placeholder="Telephone"
            textContentType="telephoneNumber"
            editable={false}
          />
          <FormField
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            name="message"
            placeholder="Message particulier"
            multiline
            numberOfLines={4}
          />
          <SubmitButton title="Commander" loading={loading} />
        </Form>
      </Content>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default ItemCommand;
