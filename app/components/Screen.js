import React from "react";
import Constants from "expo-constants";
import { StyleSheet, SafeAreaView, View, StatusBar } from "react-native";

export const ScreenNav = ({ children, style }) => {
  return (
    <SafeAreaView style={[styles.screenNav, style]}>
      <View style={[styles.view, style]}>{children}</View>
    </SafeAreaView>
  );
};

function Screen({ children, style }) {
  return (
    <SafeAreaView style={[styles.screen, style]}>
      <StatusBar backgroundColor="white" barStyle="dark-content" />

      <View style={[styles.view, style]}>{children}</View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screenNav: {
    // paddingTop: Constants.statusBarHeight,
    flex: 1,
  },
  screen: {
    // paddingTop: Constants.statusBarHeight,
    flex: 1,
  },
  view: {
    flex: 1,
  },
});

export default Screen;
