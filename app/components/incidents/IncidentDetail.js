import React from "react";
import { StyleSheet, TouchableWithoutFeedback, ScrollView, useWindowDimensions } from "react-native";
import styled from "styled-components";
import HTML from "react-native-render-html";

import { Section, SectionBody, SectionTitle } from "../../config/styles";
import colors from "../../config/colors";
import AppText from "../AppText";
import useModal from "../../hooks/useModal";
import AppModal from "../Modal";
import ImageView from "../ImageView";
import { formatUrl } from "../../utils/helpers";
import moment from "moment";
import "moment/locale/fr";
moment.locale("fr");

const IncidentUserInfo = styled.View`
  flex-direction: row;
`;
const UserPic = styled.Image`
  height: 40px;
  width: 40px;
  border-radius: 50px;
  background-color: ${colors.light};
`;
const UserInfo = styled.View`
  margin-left: 10px;
`;
const Text = styled(AppText)`
  color: ${(props) => colors[props.color]};
  font-size: ${(props) => props.size};
  ${(props) =>
    props.bold &&
    `
        font-weight: bold;
    `}
`;
const IncidentText = styled.View`
  margin-vertical: 10px;
`;
const IncidentMedias = styled.View`
  flex-direction: row;
  margin-bottom: 20px;
`;
const Media = styled.Image`
  height: 100px;
  width: 100px;
  background-color: ${colors.light};
  margin-right: 10px;
  border-radius: 5px;
`;
const IncidentStatus = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin-top: 10px;
  padding-vertical: 10px;
`;

const IncidentDetail = ({
  author,
  picture,
  text,
  date,
  status,
  images,
  videos,
  furl,
}) => {
  const imageView = useModal();
  const { width } = useWindowDimensions();
  return (
    <ScrollView style={styles.container}>
      <Section>
        <SectionBody>
          <IncidentUserInfo>
            <UserPic source={{ uri: picture }} />
            <UserInfo>
              <Text color="tertiary" size="14px" bold>
                {author}
              </Text>
              <Text color="text" size="10px">
                Riverrain
              </Text>
            </UserInfo>
          </IncidentUserInfo>
          <IncidentStatus>
            <Text color="text" size="10px">
              {moment(date).fromNow()}
            </Text>
            <Text color="primary" size="10px">
              {status}
            </Text>
          </IncidentStatus>
          <IncidentText>
            {/* <Text size="12px" color="text">
              {text}
            </Text> */}
            <HTML contentWidth={width} source={{ html: text }}  />
          </IncidentText>
          {images && (
            <>
              <SectionTitle>Photos de l'incident</SectionTitle>

              <IncidentMedias>
                {images.map((image, index) => (
                  <TouchableWithoutFeedback
                  key={index + 'klk' + Math.random()}
                    onPress={() =>
                      imageView.toggleModal(
                        `Photo ${index + 1}`,
                        <ImageView image={formatUrl(furl, image)} />
                      )
                    }
                  >
                    <Media
                      source={{ uri: formatUrl(furl, image) }}
                    />
                  </TouchableWithoutFeedback>
                ))}
              </IncidentMedias>
            </>
          )}
          {videos && (
            <>
              <SectionTitle>Videos de l'incident</SectionTitle>
            </>
          )}
        </SectionBody>
      </Section>
      <AppModal
        visible={imageView.modalVisible}
        title={imageView.modalTitle}
        content={imageView.modalContent}
        toggleModal={imageView.toggleModal}
      />
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
});
export default IncidentDetail;
