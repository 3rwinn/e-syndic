import React, { useContext } from "react";
import { StyleSheet, View } from "react-native";
import {
  Form,
  FormField,
  FormImagePicker,
  FormPicker,
  SubmitButton,
} from "../../components/forms";
import * as Yup from "yup";
import colors from "../../config/colors";
import { Section, SectionBody } from "../../config/styles";

import incidentApi from "../../api/incident";
import useAuth from "../../auth/useAuth";
import useApi from "../../hooks/useApi";
import useSnackAlert from "../../snackbar/useSnackAlert";
import { AppContext } from "../../context/AppState";
import incident from "../../api/incident";
import * as FileSystem from "expo-file-system";
// import { convertImageToBase64 } from "../../utils/helpers";

const validationSchema = Yup.object().shape({
  message: Yup.string().required("Ce champ est requis"),
  images: Yup.array().min(1, "Merci de choisir au moins une image"),
  libelid: Yup.object().required("Merci de choisir type d'incident").nullable(),
});

const incid_types = [
  {
    id: 1,
    label: "Incident des parties communes",
    value: 0,
  },
  {
    id: 2,
    label: "Incident des parties privatives",
    value: 1,
  },
];

const convertImageToBase64 = async (uri) => {
  const base64 = await FileSystem.readAsStringAsync(uri, {
    encoding: FileSystem.EncodingType.Base64,
  });
  return `data:image/jpeg;base64,${base64}`;
};

const SignalerIncident = ({ onClose }) => {
  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  // const addIncidentApi = useApi(incidentApi.addIncident);
  const [loadingBtn, setLoadingBtn] = React.useState(false);
  const snack = useSnackAlert();
  const handleSubmit = async (values, resetForm) => {
    setLoadingBtn(true);

    const photos = await Promise.all(
      values.images?.map(async (image) => {
        const convertedImage = await convertImageToBase64(image);
        return convertedImage;
      })
    );

    const newincident = {
      user_id: user.id,
      message: values.message,
      // photos: values.images,
      photos: photos,
      libelid: values.libelid,
      program: selectedProgram,
    };

    // const result = await addIncidentApi.request(newincident);
    const result = await incident.addIncident(newincident);

    setLoadingBtn(false);
    if (result.data.message === "Incident create") {
      snack.showAlert(
        "L'incident a bien été enregistré, il est en cours de traiement",
        "success"
      );
    } else {
      snack.showAlert(
        "Une erreur est survenue, merci de ré-essayer plus tard.",
        "error"
      );
    }
    resetForm();
    onClose();
    // console.log("result", result.data);
  };
  return (
    <View style={styles.container}>
      <Section>
        <SectionBody>
          <Form
            initialValues={{
              message: "",
              libelid: null,
              images: [],
            }}
            onSubmit={(values, { resetForm }) => {
              handleSubmit(values, resetForm);
            }}
            validationSchema={validationSchema}
          >
            <FormPicker
              items={incid_types}
              name="libelid"
              placeholder="Choisir un type d'incident"
              width="100%"
            />
            <FormField
              name="message"
              placeholder="Message"
              numberOfLines={3}
              multiline
            />
            <FormImagePicker name="images" placeholder="Charger les photos" />
            <SubmitButton title="SIGNALER" loading={loadingBtn} />
          </Form>
        </SectionBody>
      </Section>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default SignalerIncident;
