import React from "react";
import { TouchableWithoutFeedback } from "react-native";
import styled from "styled-components";
import { Feather } from '@expo/vector-icons'; 

import colors from "../../config/colors";
import AppText from "../AppText";

import moment from "moment";
import { shadowStyleIOS } from "../../config/styles";
moment.locale("fr");

const IncidentItemBox = styled.View`
  background-color: ${colors.white};
  width: 100%;
  border-radius: 5px;
  padding: 20px;
  elevation: 3;
  overflow: hidden;
`;

const IncidentUserInfo = styled.View`
  flex-direction: row;
`;
const UserPic = styled.Image`
  height: 40px;
  width: 40px;
  border-radius: 50px;
  background-color: ${colors.light};
`;
const UserInfo = styled.View`
  margin-left: 10px;
`;
const Text = styled(AppText)`
  color: ${(props) => colors[props.color]};
  font-size: ${(props) => props.size};
  ${(props) =>
    props.bold &&
    `
        font-weight: bold;
    `}
`;

const IncidentDetail = styled.View`
  margin-vertical: 10px;
`;

const IncidentMedias = styled.View`
  flex-direction: row;
`;
const Media = styled.Image`
  height: 100px;
  width: 100px;
  background-color: ${colors.light};
  margin-right: 10px;
  border-radius: 5px;
`;

const IncidentStatus = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin-top: 10px;
`;
const IncidentItem = ({ author, picture, text, date, status, onPress, images }) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <IncidentItemBox style={shadowStyleIOS}>
        <IncidentUserInfo>
          <UserPic source={{uri: picture}} />
          <UserInfo>
            <Text color="tertiary" size="14px" bold>
              {author}
            </Text>
            <Text color="text" size="10px">
              Riverrain
            </Text>
          </UserInfo>
        </IncidentUserInfo>
        <IncidentDetail>
          <Text size="12px" color="text">
            {text}
          </Text>
        </IncidentDetail>
        {images && (
          <Text color="text" size="14px">
            <Feather name="image" size={14} color={colors.text} /> {images.length} photos
          </Text>
        )}
        <IncidentStatus>
          <Text color="text" size="10px">
            {moment(date).fromNow()}
          </Text>
          <Text color="primary" size="10px">
            {status}
          </Text>
        </IncidentStatus>
      </IncidentItemBox>
    </TouchableWithoutFeedback>
  );
};

export default IncidentItem;
