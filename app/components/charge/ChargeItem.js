import React from "react";
import { TouchableOpacity, View } from "react-native";
import styled from "styled-components";
import colors from "../../config/colors";
import moment from "moment";
import "moment/locale/fr";
moment.locale("fr");

const ChargeBox = styled.View`
  background-color: ${colors.white};
  border-bottom-color: ${colors.light};
  border-bottom-width: 1px;
  padding: 20px;
`;

const ChargeBoxContent = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const ChargeText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: ${colors.text};
`;

const TinyText = styled.Text`
  font-size: 14px;
  color: ${colors.tertiary};
`;

const ChargePrice = styled.Text`
  align-self: flex-end;
  font-size: 14px;
  font-weight: bold;
  color: ${colors.text};
`;

const TagBox = styled.View`
  border-radius: 16px;
  width: 80px;
  margin-top: 5px;
  padding: 0px 0px 3px 0px;
  ${(props) =>
    props.color &&
    `
    background-color: ${props.color};
  `};
`;
const TagText = styled.Text`
  font-size: 12px;
  color: white;
  text-align: center;
`;

const Devise = styled.Text`
  font-size: 6px;
  align-self: flex-end;
  color: ${colors.text};

  ${(props) =>
    props.primary &&
    `
      color: ${colors.primary};
    `}
`;

const ChargeItem = ({ titre, description, status, montant, date, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <ChargeBox>
        <ChargeBoxContent>
          <View style={{ width: "50%" }}>
            <ChargeText>{titre}</ChargeText>

            <TinyText>{description}</TinyText>
            {status && (
              <TagBox color={status === "non payé" ? "red" : "green"}>
                <TagText>{status}</TagText>
              </TagBox>
            )}
          </View>
          <View>
            <Devise>FCFA</Devise>
            <ChargePrice>{montant}</ChargePrice>
            <TinyText>{moment(date).fromNow()}</TinyText>
          </View>
        </ChargeBoxContent>
      </ChargeBox>
    </TouchableOpacity>
  );
};
export default ChargeItem;
