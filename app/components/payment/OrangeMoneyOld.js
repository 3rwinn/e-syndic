import React from "react";
import { StyleSheet, View } from "react-native";
import styled from "styled-components";
import * as Yup from "yup";
import { Form, FormField, SubmitButton } from "../../components/forms";

import colors from "../../config/colors";
import { Section, SectionBody, SectionTitle } from "../../config/styles";
import AppText from "../AppText";

const Instructions = styled.View`
  padding: 10px;
`;
const Text = styled(AppText)`
  font-size: 12px;
  color: ${colors.text};
  margin-bottom: 5px;
`;

const OrangeMoneyOld = () => {
  return (
    <View style={styles.container}>
      <Section>
        <SectionBody> 
          <SectionTitle>
            Payer en sécurité avec Orange Money. Vous devez pour cela obtenir un
            code de paiement utilisable pendant 2 minutes.
          </SectionTitle>
          <Instructions>
            <Text>
              1 - Composez le #144# puis ensuite choisir 8 dans le menu et dans
              un dernier temps 2
            </Text>
            <Text>2 - Entrez votre code secret Orange Money</Text>
            <Text>
              3 - Conservez le code d'autorisation reçu par SMS. Il vous sera
              demandé pour régler votre commande par la suite
            </Text>
            <Text>
              4 - Si vous n'effectuez pas une de ces étapes, votre commande sera
              annulée.
            </Text>
            <Text>5 - Le paiement Orange Money est 100% sécurisé.</Text>
            <Text>6 - Votre code secret n’est pas communiqué.</Text>
          </Instructions>
          <Form
            initialValues={{
              phone: "",
              code: "",
            }}
            onSubmit={(values) => console.log(values)}
          >
            <FormField
              pre
              keyboardType="numeric"
              name="phone"
              placeholder="Numero de téléphone"
            />
            <FormField keyboardType="numeric" name="code" placeholder="Code" />
            <SubmitButton title="PAYER" />
          </Form>
        </SectionBody>
      </Section>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default OrangeMoneyOld;
