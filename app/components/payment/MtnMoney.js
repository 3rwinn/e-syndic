import React, { useState, useRef } from "react";
import { StyleSheet, View } from "react-native";
import styled from "styled-components";
import * as Yup from "yup";
import LottieView from "lottie-react-native";

import { Form, FormField, SubmitButton } from "../../components/forms";

import colors from "../../config/colors";
import { Section, SectionBody, SectionTitle } from "../../config/styles";
import AppText from "../AppText";

import paymentApi from "../../api/payment";
import useApi from "../../hooks/useApi";
import Button from "../Button";
import { AppContext } from "../../context/AppState";
import payment from "../../api/payment";

const Instructions = styled.View`
  padding: 10px;
`;
const Text = styled(AppText)`
  font-size: ${(props) => (props.size ? props.size : "12px")};
  color: ${colors.text};
  margin-bottom: 5px;
`;
const PaymentResponse = styled.View`
  height: 100%;
  width: 100%;
  align-items: center;
  justify-content: center;
  background: white;
  position: absolute;
  opacity: 0.9;
  z-index: 1;
`;

const validationSchema = Yup.object().shape({
  phone: Yup.string()
    .required("Merci d'indiquer votre numero mtn")
    .min(10, "Merci d'entrer un numero mtn a 10 chiffres")
    .max(10, "Merci d'entrer un numero mtn a 10 chiffres"),
});

const Response = ({ message, type, reset, extraData, event }) => {
  console.log(extraData);

  const intervalRef = useRef(null);
  const timerRef = useRef(null);

  React.useEffect(() => {
    if (type === "success" && extraData != null) {
      startPolling();
    }
    return () => {
      stopPolling();
    };
  }, [type, extraData]);

  const startPolling = () => {
    intervalRef.current = setInterval(checkPaymentStatus, 3000);
    timerRef.current = setTimeout(() => {
      stopPolling();
      event("timeout");
    }, 180000); // 3 minutes
  };

  const stopPolling = () => {
    if (intervalRef.current) {
      clearInterval(intervalRef.current);
      intervalRef.current = null;
    }
    if (timerRef.current) {
      clearTimeout(timerRef.current);
      timerRef.current = null;
    }
  };

  const checkPaymentStatus = async () => {
    const result = await payment.checkPaymenWithMtn(extraData);
    console.log("on est ici", result.data);
    if (result.ok && result.data.datas === 1) {
      event("success");
    } else if (result.data.datas === 2) {
      event("failure");
    }
  };

  return (
    <PaymentResponse>
      <LottieView
        autoPlay
        loop
        source={
          type === "success"
            ? require(`../../../assets/animations/success.json`)
            : type === "failure"
            ? require(`../../../assets/animations/failure.json`)
            : require(`../../../assets/animations/loader.json`)
        }
        style={{
          marginBottom: 20,
          height: 80,
          width: 80,
        }}
      />
      <Text size="14px">{message}</Text>
      {type !== "loader" && (
        <Button
          textColor={colors.primary}
          color={colors.white}
          title="FERMER"
          onPress={() => reset()}
        />
      )}
    </PaymentResponse>
  );
};

const MtnMoney = ({ data }) => {
  const { selectedProgram } = React.useContext(AppContext);

  console.log("DT DT DT", {
    user_id: data.real.user_id,
    charge_id: data.real.id,
  });

  // console.log("DATA TYPE", data.type);
  const payWithMobileMoney = useApi(paymentApi.payWithMtn);
  const handleSubmit = async (values, { resetForm }) => {
    const newpayment = {
      user_id: data.real.user_id,
      charge_id: data.real.id,
      type: data.type,
      numero: values.phone,
      program_id: selectedProgram,
    };

    setResponseVisibility(true);
    setResponseMsg("Chargement...");
    setResponseType("loader");

    const result = await payWithMobileMoney.request(newpayment);

    console.log("result", result.data);

    if (result.data.datas) {
      newpayment.refid = result.data.datas;

      if (result.data.datas === 0) {
        setResponseType("failure");
        setResponseMsg(
          "Une erreur est survenue veuillez ré-essayer plus tard."
        );
      } else {
        setResponseType("success");
        setResponseMsg("Veuillez confirmer la transaction.");
        setExtraData(newpayment);
      }
    }
    resetForm();
  };

  const [responseVisible, setResponseVisibility] = useState(false);
  const [responseMsg, setResponseMsg] = useState("En attente de paiement..");
  const [responseType, setResponseType] = useState("loader");
  const [extraData, setExtraData] = useState(null);

  const resetResponse = () => {
    setResponseVisibility(false);
    setResponseMsg("En attente de paiement...");
    setResponseType("loader");
    setExtraData(null);
  };

  const handlePaymentStatus = (status) => {
    setResponseVisibility(false);
    if (status === "success") {
      alert("Paiement réussie!");
    } else if (status === "timeout") {
      alert("Temps de vérification écoulé, le paiement a échoué.");
    } else {
      alert(
        "Paiement echoué, veuillez vous assurer d'avoir le montant nécessaire et ré-essayer."
      );
    }
  };

  return (
    <View style={styles.container}>
      <Section>
        <SectionBody>
          <SectionTitle>
            Payer en sécurité avec MTN Money. Suivez les étapes suivantes pour
            finaliser le paiement:
          </SectionTitle>
          <Instructions>
            <Text>1 - Saisissez votre numéro MTN dans le champ au-dessus.</Text>
            <Text>2 - Cliquez sur PAYER ;</Text>
            <Text>
              3 - Acceptez la notification reçue sur votre téléphone en entrant
              le chiffre 1 pour vous identifier. (Vous avez une minute pour
              l'accepter).
            </Text>
            <Text>
              4 - Répondez au SMS qui vous est envoyé en composant * 133# pour
              approuver la demande de paiement. (Vous avez 3 minutes pour
              répondre).
            </Text>
            <Text>
              5 - Confirmer le paiement en saisissant votre code secret MTN
              Mobile Money dans le menu qui s’ouvre sur votre téléphone.
            </Text>
          </Instructions>
          <Form
            initialValues={{
              phone: "",
            }}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            <FormField
              pre
              keyboardType="numeric"
              name="phone"
              placeholder="Numero de téléphone"
            />
            <SubmitButton title="PAYER" />
          </Form>
        </SectionBody>
      </Section>
      {responseVisible && (
        <Response
          message={responseMsg}
          type={responseType}
          reset={resetResponse}
          extraData={extraData}
          event={handlePaymentStatus}
        />
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default MtnMoney;
