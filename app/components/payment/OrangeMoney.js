import React from "react";
import { View } from "react-native";
import { WebView } from "react-native-webview";
import styled from "styled-components";
import { Ionicons } from "@expo/vector-icons";

import colors from "../../config/colors";
import { Section, SectionBody, SectionTitle } from "../../config/styles";
// import AppText from "../AppText";
import useApi from "../../hooks/useApi";
import paymentApi from "../../api/payment";

const CloseView = styled.TouchableOpacity`
  height: 80px;
  width: 80px;
  position: absolute;
  bottom: 10px;
  right: 10px;
  justify-content: center;
  align-items: center;
  z-index: 999999;
`;

const OrangeMoney = ({ data }) => {
  const realData = {
    user_id: data.real.user_id,
    charge_id: data.real.id,
    type: data.type,
  };
  const omPaymentData = useApi(paymentApi.payWithOrange);
  const [url, setUrl] = React.useState(null);
  React.useEffect(() => {
    omPaymentData.request(realData);
  }, []);
  React.useEffect(() => {
    if (
      omPaymentData.data.datas !== null ||
      omPaymentData.data.datas !== undefined
    ) {
      setUrl(omPaymentData.data.datas);
    }
  }, [omPaymentData.data]);
  return (
    <View style={{ flex: 1 }}>
      {/* <CloseView onPress={handleClose}>
        <Ionicons name="close-circle" size={50} color={colors.secondary} />
        <Text style={{ textAlign: "center" }} color="secondary" size={12} bold>
          FERMER
        </Text>
      </CloseView> */}
      {/* <View style={{ height: 40, backgroundColor: colors.primary }}></View> */}
      {url === null ||
      url === undefined ||
      url === 0 ||
      url === "" ||
      url === "0" ? (
        <View
          style={{
            marginTop: 40,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <SectionTitle>Chargement...</SectionTitle>
        </View>
      ) : (
        <WebView source={{ uri: url }} style={{ marginTop: 0 }} />
      )}
    </View>
  );
};

export default OrangeMoney;
