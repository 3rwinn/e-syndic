import React from "react";
import { Image, StyleSheet, View, Alert } from "react-native";
import styled from "styled-components";
import colors from "../../config/colors";

import { Section, SectionBody, SectionTitle } from "../../config/styles";

const PaymentList = styled.View`
  border-radius: 5px;
  background-color: ${colors.white};
  elevation: 3;
  padding-horizontal: 20px;
  padding-top: 20px;
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const PaymentItem = styled.TouchableOpacity`
  width: 47%;
  padding: 10px;
  margin-bottom: 20px;
  align-items: center;
  justify-content: center;
  border: 1px solid #dddddd;
  border-radius: 5px;
`;

const PaymentProcessorList = ({ changePaymentMethod }) => {
  const handleAlert = (type) => {
    Alert.alert(
      "Paiement",
      `Paiement par ${type} indisponible pour le moment. Veuillez-vous rendre à l'agence INOOVIM pour votre paiement ou contacter le +225 54 01 38 38.`
    );
  };
  return (
    <Section>
      <SectionBody>
        <SectionTitle>Sélectionner un moyen de paiement</SectionTitle>
        <PaymentList>
          {/* <PaymentItem onPress={() => handleAlert("carte visa")}> */}
          <PaymentItem onPress={() => changePaymentMethod("card")}>
            <Image
              height={33}
              width={129}
              source={require("../../../assets/visa-mastercard.png")}
            />
          </PaymentItem>
          <PaymentItem onPress={() => changePaymentMethod("om")}>
          {/* <PaymentItem onPress={() => handleAlert("orange money")}> */}
            <Image
              height={70}
              width={105}
              source={require("../../../assets/omoney.png")}
            />
          </PaymentItem>
          {/* <PaymentItem onPress={() => handleAlert("mtn mobile money")}> */}
          <PaymentItem onPress={() => changePaymentMethod("mm")}>
            <Image
              height={63}
              width={76}
              source={require("../../../assets/mtnmoney.png")}
            />
          </PaymentItem>
        </PaymentList>
      </SectionBody>
    </Section>
  );
};
const styles = StyleSheet.create({
  container: {},
});
export default PaymentProcessorList;
