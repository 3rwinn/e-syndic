import React from "react";
import { StyleSheet, View } from "react-native";
import * as Yup from "yup";
import { WebView } from "react-native-webview";

import colors from "../../config/colors";
import { Section, SectionBody, SectionTitle } from "../../config/styles";
import { SERVER } from "../../config/constants";
import { AppContext } from "../../context/AppState";

// const Row = styled.View`
//   flex-direction: row;
//   justify-content: space-between;
//   width: 100%;
// `;
// const Col = styled.View`
//   width: ${(props) => (props.width ? props.width : "50%")};
// `;

const VisaCard = ({ data }) => {
  const { selectedProgram } = React.useContext(AppContext);
  const [isReady, setReady] = React.useState(false);
  const [urlPayment, setUrlPayment] = React.useState("null");
  React.useEffect(() => {
    if (data.real) {
      const newvisapayment = {
        user_id: data.real.user_id,
        charge_id: data.real.id,
        type: data.type,
      };

      console.log("url",  `${SERVER}/paiement-by-bank/${newvisapayment.user_id}/${newvisapayment.charge_id}/${newvisapayment.type}/${selectedProgram}`)

      setUrlPayment(
        `${SERVER}/paiement-by-bank/${newvisapayment.user_id}/${newvisapayment.charge_id}/${newvisapayment.type}/${selectedProgram}`
      );
      setReady(true);
    }
  }, [data]);

  return (
    <View style={styles.container}>
      {isReady && (
        <WebView
          source={{ uri: urlPayment }}
          scalesPageToFit
          style={{ flex: 1, marginTop: 5 }}
          javaScriptEnabled
        />
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
});
export default VisaCard;
