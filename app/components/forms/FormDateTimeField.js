import React, { useState } from "react";
import { useFormikContext } from "formik";
import DateTimePicker from "@react-native-community/datetimepicker";
import styled from "styled-components";
import moment from "moment";
import { Platform } from "react-native";

import ErrorMessage from "./ErrorMessage";
import AppText from "../AppText";
import Button from "../Button";

const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;
const Col = styled.View`
  width: 47%;
`;

const DateText = styled(AppText)`
  font-weight: bold;
`;

const AppFormDateTimeField = ({ name, width, placeholder }) => {
  const {
    setFieldTouched,
    setFieldValue,
    errors,
    touched,
    values,
  } = useFormikContext();

  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);


    // setFieldValue(name, selectedDate);
    setFieldValue(name, moment(selectedDate).format("DD-MM-YYYY H:mm"));
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const showTimepicker = () => {
    showMode("time");
  };

  return (
    <>
      <Row>
        <AppText>{placeholder}</AppText>
        {values[name] !== undefined && typeof values[name] !== "object" && (
          <DateText>{values[name]}</DateText>
        )}
      </Row>
      <Row>
        <Col>
          <Button
            color="secondary"
            textColor="white"
            title="Choisir la date"
            onPress={showDatepicker}
          />
        </Col>
        <Col>
          <Button
            color="secondary"
            textColor="white"
            title="Choisir l'heure"
            onPress={showTimepicker}
          />
        </Col>
      </Row>
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
      <ErrorMessage error={errors[name]} visible={touched[name]} />
    </>
  );
};

export default AppFormDateTimeField;
