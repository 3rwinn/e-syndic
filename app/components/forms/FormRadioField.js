import React, { useState } from "react";
import { useFormikContext } from "formik";
import styled from "styled-components";
import colors from "../../config/colors";
import AppText from "../AppText";
import ErrorMessage from "./ErrorMessage";

const Radio = styled.TouchableOpacity`
  margin-vertical: 10px;
  height: 30px;
  width: 30px;
  background-color: ${colors.white};
  border: ${(props) => (props.bruit ? "10px" : "2px")} solid ${colors.primary};
  border-radius: 50px;
`;

const MyFlex = styled.View`
  flex-direction: ${(props) => (props.withRow ? "row" : "column")};
  align-items: ${(props) => (props.withRow ? "center" : "baseline")};
`;

function AppFormRadioField({ name, placeholder, withRow = false }) {
  const { errors, setFieldValue, touched, values } = useFormikContext();
  const [bruit, setBruit] = useState(false);

  function toggleRadio(b) {
    setBruit(b);
    setFieldValue(name, b);
  }

  return (
    <>
      <MyFlex withRow={withRow}>
        {withRow ? (
          <>
            <Radio onPress={() => toggleRadio(!bruit)} bruit={values[name]} />
            <AppText style={{paddingLeft: 10}}>{placeholder}</AppText>
          </>
        ) : (
          <>
            <AppText>{placeholder}</AppText>
            <Radio onPress={() => toggleRadio(!bruit)} bruit={values[name]} />
          </>
        )}
      </MyFlex>
      <ErrorMessage error={errors[name]} visible={touched[name]} />
    </>
  );
}

export default AppFormRadioField;
