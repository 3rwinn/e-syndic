import React from "react";
import { useFormikContext } from "formik";

import ErrorMessage from "./ErrorMessage";
import ImageInput from "../ImageInput";
import AppText from "../AppText";

export default function FormSingleImagePicker({
  name,
  borderRadius,
  placeholder,
}) {
  const { errors, setFieldValue, touched, values } = useFormikContext();
  const currentImage = values[name];

  const handleAdd = (uri) => {
    setFieldValue(name, uri);
  };

  const handleRemove = () => {
    setFieldValue(name, null);
  };

  return (
    <>
      {placeholder && <AppText>{placeholder}</AppText>}

      <ImageInput
        bordeRadius={borderRadius}
        imageUri={currentImage}
        onChangeImage={currentImage === null ? handleAdd : handleRemove}
      />
      <ErrorMessage error={errors[name]} visible={touched[name]} />
    </>
  );
}
