import React from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import styled from "styled-components";
import colors from "../config/colors";

// const Btn = styled.TouchableOpacity`
//   align-items: center
//   backgroundColor: ${(props) =>
//     props.color ? colors[props.color] : colors.primary};
//   border-radius: 8px;
//   justify-content: center;
//   margin-vertical: ${(props) => (props.mV ? props.mV : "10px")};
//   padding: 15px;
//   width: ${(props) => (props.bW ? props.bW : "100%")};
// `;

// const BtnText = styled.Text`
//   color: ${({textColor}) => colors[textColor]};
//   font-weight: bold;
//   font-size: 15px;
// `;

// const Button = ({ title, onPress, color, textColor = "white", bW, mV }) => {
//   return (
//     <Btn onPress={onPress} color={color} textColor={textColor} bW={bW} mV={mV}>
//       <BtnText>{title}</BtnText>
//     </Btn>
//   );
// };

const Button = ({
  title,
  onPress,
  color = "primary",
  textColor = "white",
  bW,
  mV = 10,
  loading = false,
}) => {
  return (
    <TouchableOpacity
      style={[
        styles.button,
        {
          backgroundColor: colors[color],
          width: bW,
          marginVertical: mV,
        },
      ]}
      onPress={onPress}
      disabled={loading}
    >
      {!loading ? (
        <Text style={[styles.text, { color: colors[textColor] }]}>{title}</Text>
      ) : (
        <View style={{width: 100}}>
          <ActivityIndicator color="#FFF" animating={loading} />
        </View>
      )}
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.primary,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    padding: 15,
    width: "100%",
    marginVertical: 10,
  },
  text: {
    color: colors.white,
    fontSize: 15,
    // textTransform: "uppercase",
    fontWeight: "bold",
  },
});

export default Button;
