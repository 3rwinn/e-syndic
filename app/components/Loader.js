import React from "react";
import { useWindowDimensions } from "react-native";
import ContentLoader, { Rect, Circle, Path } from "react-content-loader/native";

const Loader = (props) => {
  const window = useWindowDimensions();
  const mainWidth = window.width;
  return (
    <ContentLoader
      speed={1}
      width={mainWidth}
      height={124}
      viewBox={`0 0 ${mainWidth} 124`}
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      {/* <Rect x="48" y="8" rx="3" ry="3" width="88" height="6" />  */}
      {/* <Rect x="48" y="26" rx="3" ry="3" width="52" height="6" />  */}
      <Rect x="0" y="8" rx="3" ry="3" width={mainWidth - 85} height="8" />
      <Rect x="0" y="22" rx="3" ry="3" width={mainWidth - 140} height="8" />
      <Rect x="0" y="36" rx="3" ry="3" width={mainWidth - 180} height="8" />
      <Rect x="0" y="66" rx="3" ry="3" width={mainWidth - 85} height="8" />
      <Rect x="0" y="82" rx="3" ry="3" width={mainWidth - 85} height="8" />
      <Rect x="0" y="98" rx="3" ry="3" width={mainWidth - 85} height="8" />

      {/* <Circle cx="20" cy="20" r="20" /> */}
    </ContentLoader>
  );
};

export default Loader;
