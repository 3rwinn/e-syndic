import React, { useContext, useEffect, useState } from "react";
import { StyleSheet, FlatList, View } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import * as WebBrowser from "expo-web-browser";

import colors from "../../config/colors";
import styled from "styled-components";
import useAuth from "../../auth/useAuth";
import documentApi from "../../api/documents";
import useApi from "../../hooks/useApi";
import useData from "../../hooks/useData";
import AppText from "../AppText";
import { Section, SectionTitle } from "../../config/styles";
import { SERVER } from "../../config/constants";
import { AppContext } from "../../context/AppState";

const Title = styled(AppText)`
  font-size: 15px;
  color: ${colors.text};
  ${(props) =>
    props.bold &&
    `
    font-weight: bold
  `}
`;
const SubTitle = styled(AppText)`
  font-size: 8px;
  color: ${colors.tertiary};
`;
const DocumentItem = styled.TouchableOpacity`
  align-items: center;
  border: 1px solid ${colors.tertiary};
  border-radius: 5px;
  flex-direction: row;
  padding: 10px;
  margin-bottom: 10px;
  width: 100%;
`;
const DocumentContent = styled.View`
  flex-direction: column;
  margin-left: 10px;
`;

const Documents = ({ type }) => {
  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  const loadDocumentsApi = useApi(documentApi.loadUserDocuments);
  const loadDocumentsApi_2 = useApi(documentApi.loadUserDocuments);
  useEffect(() => {
    loadDocumentsApi.request(user.id, type, selectedProgram);
    if (type === "factures") {
      loadDocumentsApi_2.request(
        user.id,
        "facturesexceptionnel",
        selectedProgram
      );
    }
  }, []);

  const documents = useData(loadDocumentsApi.data, loadDocumentsApi.data.datas);

  let documents_2 = null;
  if (type === "factures") {
    documents_2 = useData(
      loadDocumentsApi_2.data,
      loadDocumentsApi_2.data.datas
    );
  }

  const handlePress = async (file_id, type) => {
    if (type === "convocations") {
      await WebBrowser.openBrowserAsync(
        `${SERVER}/mesdocuments/generer?file_id=${file_id}&type=${type}&user_id=${user.id}&program=${selectedProgram}`
      );
    } else {
      await WebBrowser.openBrowserAsync(
        `${SERVER}/mesdocuments/generer?file_id=${file_id}&type=${type}&program=${selectedProgram}`
      );
    }
  };

  return (
    <View style={styles.container}>
      {type === "factures" ? <SectionTitle>Appel de fonds</SectionTitle> : null}
      {documents.data === null ||
        (documents.data.length === 0 && (
          <Title>Aucuns fichiers pour le moment.</Title>
        ))}
      <FlatList
        key={"docs"}
        data={documents.data}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <DocumentItem
            key={item.id}
            onPress={() => handlePress(item.id, type)}
          >
            <FontAwesome5 name="file-pdf" size={30} color="red" />
            <DocumentContent>
              <Title bold>{item.name_file}</Title>
              <SubTitle>{item.created_at}</SubTitle>
            </DocumentContent>
          </DocumentItem>
        )}
      />
      {type === "factures" && documents_2.data ? (
        <>
          <SectionTitle>Charges exceptionnelles</SectionTitle>
          {documents_2.data === null ||
            (documents_2.data.length === 0 && (
              <Title>Aucuns fichiers pour le moment.</Title>
            ))}

          <FlatList
            key={"docs_2"}
            data={documents_2.data}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <DocumentItem
                key={item.id}
                onPress={() =>
                  handlePress(item.id, "facturesexceptionnel", selectedProgram)
                }
              >
                <FontAwesome5 name="file-pdf" size={30} color="red" />
                <DocumentContent>
                  <Title bold>{item.name_file}</Title>
                  <SubTitle>{item.created_at}</SubTitle>
                </DocumentContent>
              </DocumentItem>
            )}
          />
        </>
      ) : null}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 20,
  },
});
export default Documents;
