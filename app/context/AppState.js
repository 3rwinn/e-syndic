import React, { createContext, useReducer } from "react";
import AppReducer from "./AppReducer";
import AuthContext from "../auth/context";

const initalState = {
  selectedProgram: null,
  loading: false,
};

export const AppContext = createContext(initalState);

export const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initalState);

  function setLoading(bool) {
    dispatch({
      type: "SET_LOADING",
      payload: bool,
    });
  }

  function setProgram(id) {
    dispatch({
      type: "SET_PROGRAM",
      payload: id,
    });
  }

  return (
    <AppContext.Provider
      value={{
        selectedProgram: state.selectedProgram,
        loading: state.loading,
        setLoading,
        setProgram,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};
