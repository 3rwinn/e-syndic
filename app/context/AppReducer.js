export default (state, action) => {
  switch (action.type) {
    case "SET_LOADING": {
      return {
        ...state,
        loading: action.payload,
      };
    }
    case "SET_PROGRAM": {
      return {
        ...state,
        selectedProgram: action.payload,
      };
    }
  }
};
