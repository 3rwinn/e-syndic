import { useContext } from "react";

import AuthContext from "./context";
import authStorage from "./storage";

export default () => {
  const { user, setUser } = useContext(AuthContext);

  const logIn = (usr, withSession = false) => {
    setUser(usr);
    if (withSession) authStorage.storeUser(usr);
  };

  // const setProgram = (usr, selectedProgram) => {
  //   setUser({ selectedProgram, ...usr });
  // };

  const logOut = () => {
    setUser(null);
    authStorage.removeUser();
  };

  return {
    user,
    setUser,
    // setProgram,
    logIn,
    logOut,
  };
};
