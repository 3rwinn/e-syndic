import React from "react";
import { View } from "react-native";
import colors from "../config/colors";
import styled from "styled-components";
import { FontAwesome5 } from "@expo/vector-icons";
import useSnackAlert from "./useSnackAlert";
import { shadowStyleIOS } from "../config/styles";

const Text = styled.Text`
  color: ${(props) => colors[props.color] || colors.tertiary};
  font-size: ${(props) => props.size || 18}px;
  margin-left: 10px;
  ${(props) =>
    props.bold &&
    `
      font-weight: bold;
    `}
    
`;

const SnackBar = styled.View`
  width: 100%;
  min-height: 30px;
  position: absolute;
  padding-horizontal: 15px;
  bottom: 15px;
`;

const SnackContent = styled.View`
  position: relative;
  background-color: ${(props) =>
    props.status === `success`
      ? `green`
      : props.status === `error`
      ? `red`
      : `blue`};
  padding-horizontal: 15px;
  padding-vertical: 15px;
  border-radius: 5px;
  elevation: 3;
`;

const Flex = styled.View`
  flex-direction: row;
  align-items: center;
`;

const SnackAlert = () => {
  const snack = useSnackAlert();

  return (
    <>
      {snack.alert ? (
        <SnackBar>
          <SnackContent style={shadowStyleIOS} status={snack.alert.status}>
            <Flex>
              <FontAwesome5 name="info-circle" size={18} color="white" />
              <Text color="white" size={12} bold>
                {snack.alert.message}
              </Text>
            </Flex>
          </SnackContent>
        </SnackBar>
      ) : null}
    </>
  );
};

export default SnackAlert;
