import { useContext } from "react";
import SnackContext from "./context";

export default () => {
  const { alert, setAlert } = useContext(SnackContext);

  const showAlert = (message, status) => {
    setAlert({ message, status });
    setTimeout(() => {
      setAlert(null);
    }, 6000);
  };

  const hideAlert = () => {
    setAlert(null);
  };

  return {
    alert,
    setAlert,
    showAlert,
    hideAlert,
  };
};
