import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ResetPassword from "../screen/ResetPassword";
import ChangePassword from "../screen/ChangePassword";
import Checking from "../screen/newauth/Checking";
import CheckingPassword from "../screen/newauth/CheckingPassword";
import CheckingOtp from "../screen/newauth/CheckingOtp";

const Stack = createStackNavigator();

const AuthNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Connexion"
      options={{ headerShown: false }}
      component={Checking}
    />
    <Stack.Screen
      name="CheckingPassword"
      options={{ headerShown: false }}
      component={CheckingPassword}
    />
    <Stack.Screen
      name="CheckingOtp"
      options={{ headerShown: false }}
      component={CheckingOtp}
    />

    <Stack.Screen
      name="ResetPassword"
      options={{ headerShown: false }}
      component={ResetPassword}
    />
    <Stack.Screen
      name="ChangePassword"
      options={{ headerShown: false }}
      component={ChangePassword}
    />
  </Stack.Navigator>
);

export default AuthNavigator;
