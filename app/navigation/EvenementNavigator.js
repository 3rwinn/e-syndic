import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import EvenementScreen from "../screen/EvenementScreen";
import NewEvenementScreen from "../screen/NewEvenementScreen";

const Stack = createStackNavigator();

const EvenementNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Evenements"
        options={{ headerShown: false }}
        component={EvenementScreen}
      />
      <Stack.Screen
        name="AddEvenement"
        options={{ headerShown: false }}
        component={NewEvenementScreen}
      />
    </Stack.Navigator>
  );
};

export default EvenementNavigator;
