import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import IncidentsScreen from "../screen/IncidentsScreen";
import NewIncident from "../screen/NewIncident";

const Stack = createStackNavigator();

const IncidentNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Incidents"
        options={{ headerShown: false }}
        component={IncidentsScreen}
      />
      <Stack.Screen
        name="SignalerIncident"
        options={{ headerShown: false }}
        component={NewIncident}
      />
    </Stack.Navigator>
  );
};

export default IncidentNavigator;
