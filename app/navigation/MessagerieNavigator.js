import React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import Screen from "../components/Screen";
import colors from "../config/colors";
import TopBar from "../components/TopBar";

import BoiteReceptionScreen from "../screen/BoiteReceptionScreen";
import MessagesEnvoyesScreen from "../screen/MessagesEnvoyesScreen";
import NewMessage from "../components/message/NewMessage";



const Tab = createMaterialTopTabNavigator();

function MessagerieNavigator({ navigation }) {
  return (
    <Screen>
        <TopBar title="Messagerie interne" navigation={navigation} />
        <Tab.Navigator
          tabBarOptions={{
            tabStyle: {
              backgroundColor: colors.primary,
            },
            activeTintColor: colors.white,
            labelStyle: {
              fontWeight: "bold",
              fontSize: 12,
            },
            indicatorStyle: {
              backgroundColor: colors.white,
            },
          }}
        >
          <Tab.Screen
            name="Boite de reception"
            component={BoiteReceptionScreen}
            options={{ title: "Boite de reception" }}
          />
          <Tab.Screen
            name="Message envoyés"
            options={{ title: "Messages envoyés" }}
            component={MessagesEnvoyesScreen}
          />
        </Tab.Navigator>
        <NewMessage />
    </Screen>
  );
}

export default MessagerieNavigator;
