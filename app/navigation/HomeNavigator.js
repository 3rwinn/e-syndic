import React from "react";
import { StyleSheet, View } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import Screen from "../components/Screen";
import colors from "../config/colors";
import CopoprieteScreen from "../screen/CoporieteScreen";
import ExceptionnelleScreen from "../screen/ExceptionnelleScreen";
import TransactionsScreen from "../screen/TransactionsScreen";
import TopBar from "../components/TopBar";

const Tab = createMaterialTopTabNavigator();

function HomeNavigator({ navigation }) {
  return (
    <Screen>
      <View style={styles.container}>
        <TopBar title="Mes charges" navigation={navigation} />
        <Tab.Navigator
          // screenOptions={{
          //   headerShown: false,
          // }}
          // screenOptions={{
          tabBarOptions={{
            tabStyle: {
              backgroundColor: colors.primary,
            },
            activeTintColor: colors.white,
            labelStyle: {
              fontWeight: "bold",
              fontSize: 12,
            },
            indicatorStyle: {
              backgroundColor: colors.white,
            },
          }}
        >
          <Tab.Screen
            name="Copopriete"
            component={CopoprieteScreen}
            options={{ title: "Copopriété" }}
          />
          <Tab.Screen
            name="Exceptionnelles"
            options={{ title: "Exceptionne..." }}
            component={ExceptionnelleScreen}
          />
          <Tab.Screen name="Transactions" component={TransactionsScreen} />
        </Tab.Navigator>
      </View>
    </Screen>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default HomeNavigator;
