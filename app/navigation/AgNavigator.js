import React from "react";
import { StyleSheet, View } from "react-native";

import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Screen from "../components/Screen";
import colors from "../config/colors";
import TopBar from "../components/TopBar";
import AgEncours from "../screen/AgEncoursScreen";
import AgTermine from "../screen/AgTermineScreen";

const Tab = createMaterialTopTabNavigator();

const AgNavigator = ({ navigation }) => {
  return (
    <Screen>
      <View style={styles.container}>
        <TopBar title="Assemblée Générale" navigation={navigation} />
        <Tab.Navigator
          tabBarOptions={{
            tabStyle: {
              backgroundColor: colors.primary,
            },
            activeTintColor: colors.white,
            labelStyle: {
              fontWeight: "bold",
              fontSize: 12,
            },
            indicatorStyle: {
              backgroundColor: colors.white,
            },
          }}
        >
          <Tab.Screen
            name="AgEncours"
            component={AgEncours}
            options={{ title: "À venir" }}
          />
          <Tab.Screen
            name="AgTermine"
            component={AgTermine}
            options={{ title: "Passées" }}
          />
        </Tab.Navigator>
      </View>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default AgNavigator;
