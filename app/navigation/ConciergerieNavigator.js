import React from "react";
import { StyleSheet, View } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

import Screen from "../components/Screen";
import colors from "../config/colors";
import ProduitsScreen from "../screen/ProduitsScreen";
import ServicesScreen from "../screen/ServicesScreen";
import TopBar from "../components/TopBar";

const Tab = createMaterialTopTabNavigator();

const ConciergerieNavigator = ({ navigation }) => {
  return (
    <Screen>
      <View style={styles.container}>
        <TopBar title="Conciergerie" navigation={navigation} />
        <Tab.Navigator
          tabBarOptions={{
            tabStyle: {
              backgroundColor: colors.primary,
            },
            activeTintColor: colors.white,
            labelStyle: {
              fontWeight: "bold",
              fontSize: 12,
            },
            indicatorStyle: {
              backgroundColor: colors.white,
            },
          }}
        >
          <Tab.Screen
            name="Produits"
            component={ProduitsScreen}
            options={{ title: "Produits" }}
          />
          <Tab.Screen
            name="Services"
            component={ServicesScreen}
            options={{ title: "Services" }}
          />
        </Tab.Navigator>
      </View>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default ConciergerieNavigator;
