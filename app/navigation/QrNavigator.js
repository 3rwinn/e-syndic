import React, { useState, useEffect } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { View, TouchableOpacity, StyleSheet, Modal } from "react-native";
import AppText from "../components/AppText";
import Screen from "../components/Screen";
import TopBar from "../components/TopBar";
import Button from "../components/Button";
import useAuth from "../auth/useAuth";
import { Feather } from "@expo/vector-icons";
import styled from "styled-components";
import colors from "../config/colors";

// import { BarCodeScanner } from "expo-barcode-scanner";
import qrCodeApi from "../api/qrcode";
import useApi from "../hooks/useApi";
import useModal from "../hooks/useModal";
import AppModal from "../components/Modal";

import * as Linking from "expo-linking";
import { CameraView, useCameraPermissions } from "expo-camera";

//   height: 120px;
const UserBox = styled.View`
  padding: 20px;
  width: 100%;
  flex-direction: row;
  align-items: center;
  margin-bottom: 20px;
  background-color: ${colors.white};
`;
const UserImage = styled.Image`
  height: 60px;
  width: 60px;
  border-radius: 50px;
  margin-bottom: 10px;
`;
const UserInfos = styled.View`
  margin-left: 20px;
`;

const Content = styled.View`
  padding-horizontal: 20px;
  align-items: center;
  justify-content: center;
  height: 50%;
  width: 100%;
`;

const Stack = createStackNavigator();

const ScanResult = ({ data }) => {
  return (
    <View>
      <UserBox>
        <UserImage source={{ uri: data.link_img }} />
        <UserInfos>
          <AppText style={{ fontWeight: "bold" }}>{data.name}</AppText>
          <AppText>{data.email}</AppText>
          {data.lots &&
            data.lots.length > 0 &&
            data.lots.map((lot, index) => (
              <View key={index}>
                <AppText>Lot: {lot.lot}</AppText>
                <AppText>Ilot: {lot.ilot}</AppText>
              </View>
            ))}
        </UserInfos>
      </UserBox>
      <View style={{ paddingHorizontal: 20 }}>
        <Button
          title="Appeler"
          onPress={() => Linking.openURL(`tel:${data.contact}`)}
        />
        <Button
          title="Envoyer un mail"
          onPress={() => Linking.openURL(`mailto:${data.email}`)}
        />
      </View>
    </View>
  );
};

const ScanView = ({ isVisible, event, event2 }) => {
  return (
    <Modal visible={isVisible}>
      {/* <BarCodeScanner
        onBarCodeScanned={event}
        style={StyleSheet.absoluteFillObject}
      /> */}
      <CameraView
        style={StyleSheet.absoluteFillObject}
        facing={"back"}
        barcodeScannerSettings={{
          barcodeTypes: ["qr"],
        }}
        onBarcodeScanned={event}
      />
      <View style={{ position: "absolute", bottom: 30 }}>
        <TouchableOpacity onPress={() => event2()}>
          <AppText style={{ fontWeight: "bold", color: "black", margin: 20 }}>
            Quitter
          </AppText>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const QrHomeScreen = ({ navigation }) => {
  const { user, logOut } = useAuth();
  // Result scan modal
  const resultScan = useModal();

  // Qr code API
  const checkQrCodeApi = useApi(qrCodeApi.checkQrCode);
  const checkQrCode = async (data) => {
    const result = await checkQrCodeApi.request(data);
    if (result.data.datas !== 0 || result.data.datas === "0") {
      resultScan.toggleModal(
        "Informations du copopriétaire",
        <ScanResult data={result.data.datas} />
      );
    } else {
      alert("Ce code QR n'existe pas dans notre base de donnée.");
    }
  };

  // Qr code scan
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);

  const [permission, requestPermission] = useCameraPermissions();

  if (!permission) {
    // Camera permissions are still loading.
    return <View />;
  }

  // useEffect(() => {
  //   (async () => {
  //     const { status } = await BarCodeScanner.requestPermissionsAsync();
  //     setHasPermission(status === "granted");
  //   })();
  // }, []);

  const toggleModal = () => {
    setModalVisible(!modalVisible);
  };

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);
    setModalVisible(false);
    checkQrCode(data);
    console.log(
      `Bar code with type ${type} and data ${data} has been scanned!`
    );
  };

  // if (hasPermission === null) {
  //   console.log("Requesting for camera permission");
  // }
  // if (hasPermission === false) {
  //   alert("Merci d'accepter la permission pour l'accès à la camera");
  // }

  if (!permission.granted === false) {
    alert("Merci d'accepter la permission pour l'accès à la camera");
  }

  return (
    <Screen style={{ backgroundColor: colors.background }}>
      <TopBar
        navigation={navigation}
        title="Scan QR Code"
        showDrawer={false}
        specialBtn={
          <TouchableOpacity onPress={() => logOut()}>
            <Feather name="log-out" size={25} color="white" />
          </TouchableOpacity>
        }
      />
      <UserBox>
        <UserImage source={{ uri: user.img }} />
        <UserInfos>
          <AppText style={{ fontWeight: "bold" }}>{user.name}</AppText>
          <AppText>{user.email}</AppText>
        </UserInfos>
      </UserBox>

      <Content>
        <Button
          title="SCANNER UN CODE QR"
          width="100%"
          onPress={() => setModalVisible(true)}
        />
      </Content>
      <ScanView
        isVisible={modalVisible}
        event={handleBarCodeScanned}
        event2={toggleModal}
      />
      <AppModal
        visible={resultScan.modalVisible}
        content={resultScan.modalContent}
        toggleModal={resultScan.toggleModal}
        title={resultScan.modalTitle}
      />
    </Screen>
  );
};

const QrNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Scan QR CODE"
      options={{ headerShown: false }}
      component={QrHomeScreen}
    />
  </Stack.Navigator>
);

export default QrNavigator;
