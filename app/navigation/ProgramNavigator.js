import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import ChooseProgrammeScreen from "../screen/newauth/ChooseProgrammeScreen";
import AppNavigator from "../navigation/AppNavigator";

const Stack = createStackNavigator();

const ProgrammeNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ChooseProgramme"
        options={{ headerShown: false }}
        component={ChooseProgrammeScreen}
      />
      <Stack.Screen
        name="App"
        options={{ headerShown: false }}
        component={AppNavigator}
      />
    </Stack.Navigator>
  ); 
};

export default ProgrammeNavigator;
