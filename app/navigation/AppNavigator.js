import React, { useContext } from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import HomeNavigator from "./HomeNavigator";

import MessagerieNavigator from "./MessagerieNavigator";
import DocumentsScreen from "../screen/DocumentsScreen";
import ProfilScreen from "../screen/ProfilScreen";
import IncidentsScreen from "../screen/IncidentsScreen";
import NoteInformationScreen from "../screen/NoteInformationScreen";
import EvenementScreen from "../screen/EvenementScreen";
import { View } from "react-native";
import useAuth from "../auth/useAuth";

import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from "@react-navigation/drawer";
import { Feather } from "@expo/vector-icons";
import styled from "styled-components";
import AppText from "../components/AppText";
import colors from "../config/colors";
import AgNavigator from "./AgNavigator";
import ConciergerieNavigator from "./ConciergerieNavigator";
import * as WebBrowser from "expo-web-browser";
import IncidentNavigator from "./IncidentNavigator";
import EvenementNavigator from "./EvenementNavigator";
import { useNotifications } from "../hooks/useNotifications";
import { AppContext } from "../context/AppState";

const UserBox = styled.View`
  flex-direction: row;
  width: 100%;
  align-items: center;
  padding: 10px;
`;
const UserImage = styled.Image`
  height: 50px;
  width: 50px;
  border-radius: 50px;
`;
const UserInfos = styled.View`
  margin-left: 10px;
`;
const Text = styled(AppText)`
  ${(props) =>
    props.size &&
    `
    font-size: ${props.size}
  `};
  ${(props) =>
    props.bold &&
    `
    font-weight: bold
  `}
  ${(props) =>
    props.color &&
    `
    color: ${colors[props.color]}
  `}
`;

const Drawer = createDrawerNavigator();

const DrawerContent = (props) => {
  const { user, logOut } = props.auth;
  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        <UserBox>
          <UserImage source={{ uri: user?.img }} />
          <UserInfos>
            <Text size="15px" color="primary">
              {props.prog}
            </Text>
            <Text numberOfLines={1} ellipsizeMode="clip" size="11px" bold>
              {user?.prenoms} {user?.name}
            </Text>
            <Text size="10px" color="text">
              {user?.email}
            </Text>
          </UserInfos>
        </UserBox>
        <DrawerItemList {...props} />

        <DrawerItem
          label="Guide d'utilisation"
          onPress={async () =>
            await WebBrowser.openBrowserAsync(
              "https://e-syndic.inoovim.com/assets/user_guide_esyndic_v2.pdf"
            )
          }
        />
        <DrawerItem
          label="Retour aux programmes"
          onPress={() => props.navigation.navigate("ChooseProgramme")}
        />
      </DrawerContentScrollView>
      <DrawerItem
        icon={({ color, size }) => (
          <Feather color={color} size={size} name="log-out" />
        )}
        label="Se déconnecter"
        onPress={() => logOut()}
      />
    </View>
  );
};

function AppNavigator() {
  const auth = useAuth();
  const { selectedProgram } = useContext(AppContext);
  useNotifications();

  const currentProgram = auth?.user?.programmes?.find(
    (item) => item.id === selectedProgram
  )?.libelle;

  console.log("currentProgramme", currentProgram);

  // console.log("selectedProgramme", selectedProgram);

  return (
    <>
      <Drawer.Navigator
        initialRouteName="App"
        screenOptions={{
          headerShown: false,
        }}
        drawerContent={(props) => (
          <DrawerContent prog={currentProgram} auth={auth} {...props} />
        )}
      >
        <Drawer.Screen name="Mes charges" component={HomeNavigator} />
        <Drawer.Screen
          name="Messagerie interne"
          component={MessagerieNavigator}
        />
        <Drawer.Screen name="Mes documents" component={DocumentsScreen} />
        <Drawer.Screen
          name="Gestion des incidents"
          // component={IncidentsScreen}
          component={IncidentNavigator}
        />
        <Drawer.Screen
          name="Évènements"
          // component={EvenementScreen}
          component={EvenementNavigator}
        />
        <Drawer.Screen
          name="Note d'information"
          component={NoteInformationScreen}
        />
        <Drawer.Screen name="Assemblée générale" component={AgNavigator} />
        <Drawer.Screen name="Conciergerie" component={ConciergerieNavigator} />
        <Drawer.Screen name="Mon profil" component={ProfilScreen} />
        {/* <Drawer.Screen name="Notification" component={NotificationScreen} /> */}
      </Drawer.Navigator>
    </>
  );
}

export default AppNavigator;
