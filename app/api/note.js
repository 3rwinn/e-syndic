import client from "./client";

const loadUserCityNotes = (user_id, program_id) =>
  client.get(`/notes/informations?user_id=${user_id}&program=${program_id}`);

export default {
  loadUserCityNotes,
};
