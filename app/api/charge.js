import client from "./client";

const loadUserCharges = (user_id, program_id) =>
  client.get(`/getcharge?user_id=${user_id}&program=${program_id}`);

export default {
  loadUserCharges,
};
