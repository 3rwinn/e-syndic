import client from "./client";

const payWithMtn = (paymentData) => {
  // const dataTosend = new FormData();
  // dataTosend.append("numero", paymentData.numero);
  // dataTosend.append("user_id", paymentData.user_id);
  // dataTosend.append("charge_id", paymentData.charge_id);
  // dataTosend.append("type", paymentData.type);

  return client.post(
    `/paiement-by-mtnmoney?numero=${paymentData.numero}&user_id=${paymentData.user_id}&charge_id=${paymentData.charge_id}&type=${paymentData.type}&program=${paymentData.program_id}`
  );
};

const payWithOrange = (paymentData) => {
  return client.post(
    `/paiement-by-orange?user_id=${paymentData.user_id}&charge_id=${paymentData.charge_id}&type=${paymentData.type}`
  );
};

const checkPaymenWithMtn = (paymentData) => {
  const newPayData = {
    program: paymentData.program_id,
    ...paymentData,
  };

  console.log("newData", newPayData);

  return client.post(`/paiement-by-mtnmoney/status`, newPayData);
};

export default {
  payWithMtn,
  payWithOrange,
  checkPaymenWithMtn,
};
