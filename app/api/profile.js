import client from "./client";

const endpoint = "/update_profil";

const updateProfile = (user) => {
  // const data = new FormData();
  // data.append("user_id", user.user_id);
  // data.append("nom", user.nom);
  // data.append("prenom", user.prenom);
  // data.append("contact", user.contact);
  // data.append("contact2", user.contact2);
  // data.append("adresse", user.adresse);
  // data.append("smatrimoniale", user.smatrimoniale);

  // return client.post(`${endpoint}`, data);
  return client.post(`${endpoint}`, user);
};

export default {
  updateProfile,
};
