import { Form } from "formik";
import client from "./client";

const endpoint = "/incidents";

const loadUserCityIncidents = (user_id, program_id) =>
  client.get(`${endpoint}?user_id=${user_id}&program=${program_id}`);

// const addIncident = (incident) =>
//   client.post(
//     `${endpoint}/create?user_id=${incident.user_id}&message=${incident.message}&photos=${incident.photos}`
//   );

const addIncident = (incident) => {
  // console.log(incident);
  // const data = new FormData();
  // data.append("user_id", incident.user_id);
  // data.append("message", incident.message);
  // data.append("libelid", incident.libelid?.value);
  // data.append("program", incident.program);

  // incident.photos.forEach((image, index) =>
  //   data.append("photos[]", {
  //     name: "incident-photo" + index,
  //     type: "image/jpeg",
  //     uri: image,
  //   })
  // );

  let newDatas = {
    user_id: incident.user_id,
    message: incident.message,
    libelid: incident.libelid?.value,
    program: incident.program,
    photos: incident.photos,
  };

  return client.post(`${endpoint}/create`, newDatas);
};

export default {
  loadUserCityIncidents,
  addIncident,
};
