import client from "./client";

const endpoint = "/evenements";

const loadUserCityEvenements = (user_id, program_id) =>
  client.get(`${endpoint}?user_id=${user_id}&program=${program_id}`);

const addEvenement = (evenement) => {
  // const data = new FormData();
  // data.append("user_id", evenement.user_id);
  // data.append("title", evenement.title);
  // data.append("desc", evenement.desc);
  // data.append("photo", {
  //   name: "evenement-photo-" + Math.random() + "-" + Date.now(),
  //   type: "image/jpeg",
  //   uri: evenement.photo,
  // });
  // data.append("ddebut", evenement.ddebut);
  // data.append("dfin", evenement.dfin);
  // data.append("bruit", evenement.bruit);
  // data.append("program", evenement.program);

  const datas = {
    user_id: evenement.user_id,
    title: evenement.title,
    desc: evenement.desc,
    photo: evenement.photo,
    ddebut: evenement.ddebut,
    dfin: evenement.dfin,
    bruit: evenement.bruit,
    program: evenement.program,
  };

  return client.post(`${endpoint}/create`, datas);
};

export default {
  loadUserCityEvenements,
  addEvenement,
};
