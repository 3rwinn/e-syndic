import client from "./client";

const login = (email, password) =>
  client.get(`/login?email=${email}&password=${password}`);

const resetPassword = (email) =>
  client.post(`/motdepasse/perdu?email=${email}`);

const changeDefaultPassword = (
  user_id,
  oldpassword,
  newpassword,
  confirmpassword
) =>
  client.post(
    `/update/password?user_id=${user_id}&oldpassword=${oldpassword}&newpassword=${newpassword}&confirmpassword=${confirmpassword}`
  );

const checkEmail = (email) => client.get(`login?email=${email}`);

const checkPassword = (email, password) =>
  client.get(`/login/two?email=${email}&password=${password}`);

const checkOtp = (email, otp) =>
  client.get(`/check/otp?email=${email}&otp=${otp}`);

const changeLostPass = (email, newpassword) =>
  client.post(`/update/pass/lost?email=${email}&newpassword=${newpassword}`);

const changePasswordWithOTP = (email, otp, newpassword) =>
  client.get(`/login/otp?email=${email}&otp=${otp}&newpassword=${newpassword}`);

const reloadOTP = (email) => client.get(`/login/otp/reload?email=${email}`);

const saveToken = (user_id, token) =>
  client.post(`/send/token/expo?token=${token}&user_id=${user_id}`);

export default {
  login,
  resetPassword,
  changeDefaultPassword,
  checkEmail,
  checkPassword,
  changePasswordWithOTP,
  reloadOTP,
  saveToken,
  checkOtp,
  changeLostPass,
};
