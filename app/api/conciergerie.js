import client from "./client";

const endpoint = "/conciergeries";

const loadProducts = (user_id, program_id) =>
  client.get(`${endpoint}?user_id=${user_id}&program=${program_id}`);

const askProduct = (user_id, pdt_id, demande, program_id) =>
  client.post(
    `${endpoint}/demandes?user_id=${user_id}&pdt_id=${pdt_id}&demande=${demande}&program=${program_id}`
  );

export default {
  loadProducts,
  askProduct,
};
