import client from "./client";

const endpoint = "/messages";

const loadUserMessagesReceived = (user_id, program_id) =>
  client.get(`${endpoint}/received?user_id=${user_id}&program=${program_id}`);

const loadUserMessagesSent = (user_id, program_id) =>
  client.get(`${endpoint}/send?user_id=${user_id}&program=${program_id}`);

const sendMessage = (message) => {
  console.log(
    `${endpoint}/create?user_id=${message.user_id}&objet=${message.objet}&contenu=${message.contenu}&program=${message.program}`
  );
  return client.post(
    `${endpoint}/create?user_id=${message.user_id}&objet=${message.objet}&contenu=${message.contenu}&program=${message.program}`
  );
};

export default {
  loadUserMessagesReceived,
  loadUserMessagesSent,
  sendMessage,
};
