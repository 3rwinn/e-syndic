import client from "./client";

const checkQrCode = (data) => client.get(`/scanne/qrcode?code=${data}`);

export default {
  checkQrCode,
};
