import client from "./client";

const endpoint = "/get/ags";

const loadUserAgs = (user_id, program_id) =>
  client.get(`${endpoint}?user_id=${user_id}&program=${program_id}`);

const loadUserAgItems = (user_id, ag_id, program_id) =>
  client.get(
    `${endpoint}/item?user_id=${user_id}&ag_id=${ag_id}&program=${program_id}`
  );

const loadUserAgVote = (user_id, ag_id, program_id) =>
  client.get(
    `${endpoint}/vote?user_id=${user_id}&ag_id=${ag_id}&program=${program_id}`
  );

const addVote = (user_id, pjt_id, ag_id, vote, program_id) =>
  client.post(
    `${endpoint}/sendvote?user_id=${user_id}&pjt_id=${pjt_id}&ag_id=${ag_id}&result=${vote}&program=${program_id}`
  );

export default {
  loadUserAgs,
  loadUserAgItems,
  loadUserAgVote,
  addVote,
};
