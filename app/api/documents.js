import client from "./client";

const endpoint = "/mesdocuments";

const loadUserDocuments = (user_id, type, program_id) =>
  client.get(`${endpoint}?user_id=${user_id}&type=${type}&program=${program_id}`);

export default {
  loadUserDocuments,
};
