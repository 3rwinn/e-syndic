import React from "react";
import { FlatList, View } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";
import styled from "styled-components";
import Loader from "../components/Loader";
import {
  Container,
  EmptyContent,
  EmptyContentBox,
  Section,
  SectionBody,
} from "../config/styles";
import AppText from "../components/AppText";

import colors from "../config/colors";
import AgItem from "../components/assembleg/AgItem";

import useModal from "../hooks/useModal";
import AppModal from "../components/Modal";
import AgDetail from "../components/assembleg/AgDetail";
import useApi from "../hooks/useApi";
import agApi from "../api/ag";
import useData from "../hooks/useData";
import useAuth from "../auth/useAuth";
import { AppContext } from "../context/AppState";

const Box = styled.View`
  height: 150px;
  width: 100%;
  background-color: white;
  border-radius: 5px;
  padding: 20px;
  elevation: 3;
  margin-bottom: 20px;
`;

export const LoadingContent = () => (
  <>
    <Box>
      <Loader />
    </Box>
    <Box>
      <Loader />
    </Box>
    <Box>
      <Loader />
    </Box>
  </>
);

const AgEncoursScreen = () => {
  const agModal = useModal();
  // const [isLoading, setLoading] = React.useState(true);

  const { user } = useAuth();
  const { selectedProgram } = React.useContext(AppContext);
  const loadAgsApi = useApi(agApi.loadUserAgs);

  React.useEffect(() => {
    loadAgsApi.request(user.id, selectedProgram);
  }, []);

  const ags = useData(loadAgsApi.data, loadAgsApi.data.datas);

  const [refresh, setRefreshing] = React.useState(false);

  return (
    <Container>
      {loadAgsApi.loading ? (
        <Section>
          <SectionBody>
            <LoadingContent />
          </SectionBody>
        </Section>
      ) : ags.data === null ||
        (ags.data !== null && ags.data.agencours.length === 0) ? (
        <EmptyContent
          message={`Aucune assemblée générale pour le \nmoment.`}
          retry={() => loadAgsApi.request(user.id, selectedProgram)}
        />
      ) : (
        ags.data !== null && (
          <FlatList
            style={{ paddingTop: 20 }}
            data={ags.data.agencours}
            refreshing={refresh}
            keyExtractor={(item) => item.id.toString()}
            onRefresh={() => loadAgsApi.request(user.id, selectedProgram)}
            renderItem={({ item }) => (
              <View style={{ marginBottom: 20, marginHorizontal: 20 }}>
                <AgItem
                  onPress={() =>
                    agModal.toggleModal(
                      "Contenu de l'AG",
                      <AgDetail
                        id={item.agprogress.id}
                        type={item.typeag}
                        titre={item.agprogress.titre}
                        date={item.agprogress.date}
                        heure={item.agprogress.heure}
                        lieu={item.agprogress.lieu}
                      />
                    )
                  }
                  type={item.typeag}
                  titre={item.agprogress.titre}
                  date={item.agprogress.date}
                  heure={item.agprogress.heure}
                  lieu={item.agprogress.lieu}
                />
              </View>
            )}
          />
        )
      )}

      <AppModal
        visible={agModal.modalVisible}
        toggleModal={agModal.toggleModal}
        title={agModal.modalTitle}
        content={agModal.modalContent}
      />
    </Container>
  );
};

export default AgEncoursScreen;
