import React from "react";
import { StyleSheet, FlatList, TouchableOpacity, View } from "react-native";
import styled from "styled-components";
import { Feather } from "@expo/vector-icons";

import Screen from "../components/Screen";
import TopBar from "../components/TopBar";
import {
  Container,
  Section,
  SectionBody,
  SectionTitle,
} from "../config/styles";
import AppText from "../components/AppText";
import colors from "../config/colors";

import useModal from "../hooks/useModal";
import AppModal from "../components/Modal";
import Documents from "../components/documents/Documents";

const CategoryItem = styled.TouchableOpacity`
  align-items: center;
  justify-content: center;
  border: 1px solid ${colors.tertiary};
  border-radius: 5px;
  height: 120px;
  margin-right: 20px;
  margin-bottom: 20px;
  width: 47%;
`;
const Title = styled(AppText)`
  font-size: 15px;
  color: ${colors.text};
  ${(props) =>
    props.bold &&
    `
    font-weight: bold
  `}
`;
const SubTitle = styled(AppText)`
  font-size: 8px;
  color: ${colors.tertiary};
`;
const DocumentItem = styled.TouchableOpacity`
  align-items: center;
  border: 1px solid ${colors.tertiary};
  border-radius: 5px;
  flex-direction: row;
  padding: 10px;
  margin-bottom: 10px;
  width: 100%;
`;
const DocumentContent = styled.View`
  flex-direction: column;
  margin-left: 10px;
`;

const HeaderNavigation = styled.View`
  flex-direction: row;
`;

const categories = [
  {
    label: "Mes documents",
    type: "documents",
  },
  {
    label: "Mes factures",
    type: "factures",
  },
  {
    label: "Points financiers",
    type: "points",
  },
  {
    label: "Budgets",
    type: "budgets",
  },
  {
    label: "Notes d'information",
    type: "notes",
  },
  {
    label: "Procès-verbaux",
    type: "pv",
  },
  {
    label: "Convocations",
    type: "convocations",
  },
];

const DocumentsScreen = ({ navigation }) => {
  const fileModal = useModal();
  return (
    <Screen>
      <TopBar title="Mes documents" navigation={navigation} />
      <Container>
        <Section>
          <SectionBody>
            <HeaderNavigation>
              <TouchableOpacity
                onPress={() => {
                  setFolderOpen(false);
                  setCategory(null);
                }}
              >
                <SectionTitle>Catégories</SectionTitle>
              </TouchableOpacity>
            </HeaderNavigation>

            <FlatList
              key={"cats"}
              keyExtractor={(item) => "cats_" + item.label}
              data={categories}
              // keyExtractor={(item) => item.label.toString()}
              numColumns={2}
              renderItem={({ item }) => (
                <CategoryItem
                  key={item.label}
                  onPress={() => {
                    fileModal.toggleModal(
                      `${item.label} (fichiers)`,
                      <Documents type={item.type} />
                    );
                  }}
                >
                  <Feather size={60} name="folder" color={colors.tertiary} />
                  <Title>{item.label}</Title>
                </CategoryItem>
              )}
            />
          </SectionBody>
        </Section>
        <AppModal
          visible={fileModal.modalVisible}
          toggleModal={fileModal.toggleModal}
          title={fileModal.modalTitle}
          content={fileModal.modalContent}
        />
      </Container>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {},
});
export default DocumentsScreen;
