import React, { useContext, useState } from "react";
import { FlatList, View } from "react-native";

import {
  Container,
  Section,
  SectionBody,
  SectionTitle,
  Statistiques,
  StatBox,
  StatTitle,
  StatDesc,
  Devise,
  EmptyContent,
  shadowStyleIOS,
} from "../config/styles";
import ChargeItem from "../components/charge/ChargeItem";
import AppModal from "../components/Modal";
import usePaymentMethod from "../hooks/usePaymentMethod";
import useApi from "../hooks/useApi";
import chargeApi from "../api/charge";
import useAuth from "../auth/useAuth";
import { formatToMoney } from "../utils/helpers";
import ChargeLoader from "../components/charge/ChargeLoader";
import { AppContext } from "../context/AppState";

const CopoprieteScreen = () => {
  const [modalVisible, setModalVisible] = React.useState(false);
  function toggleModal() {
    setModalVisible(!modalVisible);
  }
  const [data, setData] = useState(null);
  const modalData = usePaymentMethod(data);
  const showModal = (data) => {
    setData({
      real: data,
      type: "appel",
    });
    toggleModal();
  };

  // Load user charges
  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  const [chargeData, setChargeData] = React.useState(null);
  const chargeApiData = useApi(chargeApi.loadUserCharges);

  React.useEffect(() => {
    chargeApiData.request(user.id, selectedProgram);
  }, []);

  React.useEffect(() => {
    if (chargeApiData.data.datas) {
      setChargeData(chargeApiData.data.datas.charges);
    }
  }, [chargeApiData.data]);

  // Refresh and reset
  const [refreshing, setRefresh] = React.useState(false);
  function reset() {
    modalData.changePaymentMethod("");
    chargeApiData.request(user.id, selectedProgram);
  }
  return (
    <Container>
      <View>
        {chargeApiData.loading ? (
          <ChargeLoader />
        ) : chargeData !== null && chargeData.length === 0 ? (
          <EmptyContent
            message="Aucunes charges pour le moment"
            retry={() => chargeApiData.request(user.id)}
          />
        ) : (
          chargeData && (
            <FlatList
              ListHeaderComponent={
                <>
                  <Section>
                    <SectionBody>
                      <SectionTitle>Rapport général</SectionTitle>
                      {chargeData && (
                        <Statistiques>
                          <StatBox style={shadowStyleIOS}>
                            <Devise>FCFA</Devise>
                            <StatTitle>
                              {formatToMoney(chargeData.montantpayee)}
                            </StatTitle>
                            <StatDesc>Charges de copropriété payées</StatDesc>
                          </StatBox>

                          <StatBox style={shadowStyleIOS}>
                            <Devise>FCFA</Devise>
                            <StatTitle>
                              {formatToMoney(chargeData.montantimpayee)}
                            </StatTitle>
                            <StatDesc>Charges de copropriété impayées</StatDesc>
                          </StatBox>
                        </Statistiques>
                      )}
                    </SectionBody>
                  </Section>
                  <Section>
                    <SectionBody>
                      <SectionTitle>Mes charges de copropriété</SectionTitle>
                    </SectionBody>
                  </Section>
                </>
              }
              refreshing={refreshing}
              onRefresh={() => chargeApiData.request(user.id, selectedProgram)}
              data={chargeData.listecharge}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => (
                <ChargeItem
                  onPress={() => {
                    item.status === 0 ? showModal(item) : null;
                  }}
                  titre={item.appelfond.libelle}
                  description={`Lot: ${item.lot.lot} | Budget ${item.budget.annee}`}
                  status={item.status === 0 ? "non payé" : "payé"}
                  montant={formatToMoney(item.solde)}
                  date={item.datefin}
                />
              )}
            />
          )
        )}
      </View>
      <AppModal
        visible={modalVisible}
        toggleModal={toggleModal}
        title="Paiement"
        content={modalData.modalContent}
        reset={reset}
      />
    </Container>
  );
};
export default CopoprieteScreen;
