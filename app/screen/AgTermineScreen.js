import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import {
  Container,
  EmptyContent,
  Section,
  SectionBody,
} from "../config/styles";
import styled from "styled-components";
import useModal from "../hooks/useModal";
import useAuth from "../auth/useAuth";
import useApi from "../hooks/useApi";
import agApi from "../api/ag";
import useData from "../hooks/useData";
import { LoadingContent } from "./AgEncoursScreen";
import AgItem from "../components/assembleg/AgItem";
import AppModal from "../components/Modal";
import AgDetail from "../components/assembleg/AgDetail";
import { AppContext } from "../context/AppState";

// const EmptyContentBox = styled.View`
//   flex: 1;
//   align-items: center;
//   justify-content: center;
// `;

const AgTermineScreen = () => {
  const agModal = useModal();
  const { user } = useAuth();
  const { selectedProgram } = React.useContext(AppContext);
  const loadAgsApi = useApi(agApi.loadUserAgs);

  React.useEffect(() => {
    loadAgsApi.request(user.id, selectedProgram);
  }, []);

  const ags = useData(loadAgsApi.data, loadAgsApi.data.datas);

  const [refresh, setRefreshing] = React.useState(false);

  return (
    <Container>
      {loadAgsApi.loading ? (
        <Section>
          <SectionBody>
            <LoadingContent />
          </SectionBody>
        </Section>
      ) : ags.data !== null && ags.data.agold.length === 0 ? (
        <EmptyContent
          message={`Aucune assemblée générale pour le \nmoment.`}
          retry={() => loadAgsApi.request(user.id, selectedProgram)}
        />
      ) : (
        ags.data !== null && (
          <FlatList
            style={{ paddingTop: 20 }}
            data={ags.data.agold}
            refreshing={refresh}
            keyExtractor={(item) => item.id.toString()}
            onRefresh={() => loadAgsApi.request(user.id, selectedProgram)}
            renderItem={({ item }) => (
              <View style={{ marginBottom: 20, marginHorizontal: 20 }}>
                <AgItem
                  onPress={() =>
                    agModal.toggleModal(
                      "Contenu de l'AG",
                      <AgDetail
                        id={item.agold.id}
                        type={item.typeag}
                        titre={item.agold.titre}
                        date={item.agold.date}
                        heure={item.agold.heure}
                        lieu={item.agold.lieu}
                      />
                    )
                  }
                  type={item.typeag}
                  titre={item.agold.titre}
                  date={item.agold.date}
                  heure={item.agold.heure}
                  lieu={item.agold.lieu}
                />
              </View>
            )}
          />
        )
      )}
      <AppModal
        visible={agModal.modalVisible}
        toggleModal={agModal.toggleModal}
        title={agModal.modalTitle}
        content={agModal.modalContent}
      />
    </Container>
  );
};
const styles = StyleSheet.create({
  container: {},
});
export default AgTermineScreen;
