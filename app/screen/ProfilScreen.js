import React, { useState } from "react";
import { ScrollView, StyleSheet, View, Pressable, Text } from "react-native";
import * as Yup from "yup";
import styled from "styled-components";

import Screen from "../components/Screen";
import TopBar from "../components/TopBar";
import { Section, SectionBody, SectionTitle } from "../config/styles";
import { Form, FormField, SubmitButton } from "../components/forms";

import useAuth from "../auth/useAuth";
import profileApi from "../api/profile";
import authApi from "../api/auth";
import useApi from "../hooks/useApi";
import useSnackAlert from "../snackbar/useSnackAlert";
import profile from "../api/profile";
import colors from "../config/colors";

const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;
const Col = styled.View`
  flex-direction: column;
  width: 47%;
`;

// const validationSchema = Yup.object().shape({
//   nom: Yup.string().required("Ce champ est requis"),
//   prenoms: Yup.string().required("Ce champ est requis"),
//   // email: Yup.string().required("Ce champ est requis"),
//   phone_1: Yup.string().required("Ce champ est requis"),
//   // phone_2: Yup.string().required("Ce champ est requis"),
//   // adresse: Yup.string().required("Ce champ est requis"),
//   status: Yup.string().required("Ce champ est requis"),
// });

const validationSchemaPass = Yup.object().shape({
  password: Yup.string().required("Ce champ est requis"),
  new_password: Yup.string()
    .required("Ce champ est requis")
    .min(6, "Le mot de passe doit contenir 6 caractères"),
  confirm_password: Yup.string()
    .required("Ce champ est requis")
    .oneOf(
      [Yup.ref("new_password"), null],
      "Les mots de passe doivent être identiques"
    ),
});

const SpecialTabView = ({ activeTab, setActiveTab }) => {
  return (
    <View
      style={{
        flexDirection: "row",
        width: "100%",
        backgroundColor: colors.white,
        // paddingHorizontal: 20,
        paddingBottom: 20,
      }}
    >
      <Pressable
        onPress={() => setActiveTab(0)}
        style={{
          padding: 10,
          maxWidth: 350,
          backgroundColor: activeTab === 0 ? colors.primary : colors.secondary,
          borderRadius: 10,
          marginRight: 10,
        }}
      >
        <Text style={{ color: colors.white, fontSize: 16, fontWeight: "bold" }}>
          Infos du compte
        </Text>
      </Pressable>
      <Pressable
        onPress={() => setActiveTab(1)}
        style={{
          padding: 10,
          maxWidth: 350,
          backgroundColor: activeTab === 1 ? colors.primary : colors.secondary,
          borderRadius: 10,
          marginRight: 10,
        }}
      >
        <Text style={{ color: colors.white, fontSize: 16, fontWeight: "bold" }}>
          Securité
        </Text>
      </Pressable>
    </View>
  );
};

const ProfilScreen = ({ navigation }) => {
  const { user, setUser } = useAuth();
  const [profilData, setProfilData] = useState(user);
  const updateProfileApi = useApi(profileApi.updateProfile);
  const updatePasswordApi = useApi(authApi.changeDefaultPassword);
  const snack = useSnackAlert();
  const [loading, setLoading] = useState();
  const [loadingPass, setLoadingPass] = useState();
  const [activeTab, setActiveTab] = React.useState(0);
  // console.log(profilData)
  const handleSubmit = async (values) => {
    const nuser = {
      user_id: user.id,
      nom: values.nom,
      prenom: values.prenoms,
      contact: values.phone_1,
      contact2: values.phone_2,
      adresse: values.adresse,
      smatrimoniale: values.situation,
    };
    setLoading(true);
    // const result = await updateProfileApi.request(nuser);
    const result = await profile.updateProfile(nuser);

    console.log("RESULT", result);

    setLoading(false);
    if (result.data.message === "success updated") {
      const user_updated = result.data.data;
      user_updated.img = user.img;

      setUser(result.data.data);
      setProfilData(result.data.data);
      snack.showAlert("Profil modifié avec succès", "success");
    } else {
      snack.showAlert(
        "Une erreur est survenue, merci de ré-essayer plus tard",
        "error"
      );
    }
  };

  const handlePasswordChange = async (values) => {
    setLoadingPass(true);
    const result = await updatePasswordApi.request(
      user.id,
      values.password,
      values.new_password,
      values.confirm_password
    );
    setLoadingPass(false);
    if (result.data.message === "success") {
      snack.showAlert("Mot de passe modifié avec succès", "success");
    } else {
      result.data.message !== null
        ? snack.showAlert(result.data.message, "error")
        : snack.showAlert(
            "Une erreur est survenue, merci de ré-essayer plus tard",
            "error"
          );
    }

    console.log("ref", result.data);
  };

  return (
    <Screen>
      <TopBar title="Mon profil" navigation={navigation} />
      <ScrollView>
        <Section>
          <SectionBody>
            <SpecialTabView activeTab={activeTab} setActiveTab={setActiveTab} />

            {activeTab === 0 && (
              <>
                <SectionTitle style={{ fontSize: 16 }}>Mon profil</SectionTitle>
                <Form
                  initialValues={{
                    nom: profilData.name,
                    prenoms: profilData.prenoms,
                    email: profilData.email,
                    phone_1: profilData.contact,
                    phone_2:
                      profilData.contact_second === "null"
                        ? ""
                        : profilData.contact_second,
                    adresse:
                      profilData.adresse === "null" ? "" : profilData.adresse,
                    situation: profilData.situation,
                  }}
                  onSubmit={handleSubmit}
                >
                  <FormField name="nom" placeholder="Nom" />
                  <FormField name="prenoms" placeholder="Prenoms" />
                  {/* <FormField
                name="email"
                placeholder="Adresse email"
                editable={false}
              /> */}
                  <FormField name="adresse" placeholder="Adresse" />
                  <Row>
                    <Col>
                      <FormField
                        name="phone_1"
                        placeholder="Telephone"
                        keyboardType="number-pad"
                      />
                    </Col>
                    <Col>
                      <FormField
                        name="phone_2"
                        placeholder="Telephone 2"
                        keyboardType="number-pad"
                      />
                    </Col>
                  </Row>

                  <FormField
                    name="situation"
                    placeholder="Situation matrimonial"
                  />
                  <SubmitButton title="Modifier" loading={loading} />
                </Form>
              </>
            )}

            {activeTab === 1 && (
              <>
                <SectionTitle style={{ fontSize: 16, marginTop: 5 }}>
                  Changement de mot de passe
                </SectionTitle>
                <Form
                  initialValues={{
                    password: "",
                    new_password: "",
                    confirm_password: "",
                  }}
                  validationSchema={validationSchemaPass}
                  onSubmit={handlePasswordChange}
                >
                  <FormField
                    name="password"
                    placeholder="Mot de passe actuel"
                    secureTextEntry
                  />
                  <FormField
                    name="new_password"
                    placeholder="Nouveau mot de passe"
                    secureTextEntry
                  />
                  <FormField
                    name="confirm_password"
                    placeholder="Confirmer le nouveau mot de passe"
                    secureTextEntry
                  />
                  <SubmitButton title="Changer" loading={loadingPass} />
                </Form>
              </>
            )}
          </SectionBody>
        </Section>
      </ScrollView>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {},
});
export default ProfilScreen;
