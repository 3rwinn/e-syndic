import React from "react";
import AddEvenement from "../components/evenements/AddEvenement";
import Screen from "../components/Screen";
import TopBar from "../components/TopBar";
import { Container } from "../config/styles";

const NewEvenementScreen = ({ navigation }) => {
  return (
    <Screen>
      <TopBar
        title="Créer un évènement"
        navigation={navigation}
        isFakeModal={true}
      />
      <Container>
          <AddEvenement />
      </Container>
    </Screen>
  );
};

export default NewEvenementScreen;
