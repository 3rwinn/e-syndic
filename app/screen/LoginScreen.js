import React, { useEffect, useState } from "react";
import { StyleSheet, View, Keyboard } from "react-native";
import styled from "styled-components";

import AppText from "../components/AppText";
import Button from "../components/Button";
import colors from "../config/colors";
import Screen from "../components/Screen";

import * as Yup from "yup";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import useAuth from "../auth/useAuth";
import authApi from "../api/auth";

const Container = styled.View`
  flex: 1;
  padding-horizontal: 20px;
  justify-content: center;
  align-items: center;
`;

const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  text-align: center;

  ${(props) =>
    props.primary &&
    `
    color: ${colors.primary}
  `}
`;

const DescBox = styled.View`
  width: 80%;
  margin-vertical: 20px;
`;

const Desc = styled(AppText)`
  text-align: center;
  font-size: 15px;
`;

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required("Ce champs est requis")
    .email("Merci d'indiquer une adresse email valide"),
  password: Yup.string().required("Ce champ est requis"),
});

const LoginScreen = ({ navigation }) => {
  const [isVisible, setVisible] = useState(true);

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", show);
    Keyboard.addListener("keyboardDidHide", remove);

    return () => {
      Keyboard.addListener("keyboardDidShow", show);
      Keyboard.addListener("keyboardDidHide", remove);
    };
  }, []);

  const show = () => {
    setVisible(false);
  };
  const remove = () => {
    setVisible(true);
  };

  const auth = useAuth();
  const [loginFailed, setLoginFailed] = useState(false);
  const [loading, setLoading] = useState(false); 

  const handleSubmit = async (email, password) => {
    const result = await authApi.login(email, password);
    setLoading(false);
    if (result.data.user === 0) return setLoginFailed(true);
    setLoginFailed(false);
    if (result.data.forcepass === "oui") {
      const user = {
        data: result.data.user,
        old_password: password,
      };
      // Rediriger vers la page de modification mot de passe.
      navigation.navigate("ChangePassword", { user });
    } else {
      auth.logIn(result.data.user);
    }
  };

  return (
    <Screen>
      <Container>
        <View
          style={[styles.mainText, { display: isVisible ? "flex" : "none" }]}
        >
          <Title>
            Accédez à votre proprieté en toute <Title primary>QUIÉTUDE</Title>
          </Title>
        </View>
        <DescBox>
          {!loginFailed && (
            <Desc>
              Bienvenue sur l'application de gestion du syndic de copropriété.
              Veuillez-vous identifier pour y accéder.
            </Desc>
          )}
          <ErrorMessage
            error="Email ou mot de passe invalide."
            visible={loginFailed}
          />
        </DescBox>
        <View style={styles.form}>
          <Form
            initialValues={{ email: "", password: "" }}
            onSubmit={({ email, password }) => {
              setLoading(true);
              handleSubmit(email, password);
            }}
            validationSchema={validationSchema}
          >
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              name="email"
              placeholder="Adresse email"
              textContentType="emailAddress"
            />
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              name="password"
              placeholder="Mot de passe"
              secureTextEntry
              textContentType="password"
            />
            <SubmitButton title="Connexion" loading={loading} />
            <Button
              title="Mot de passe oublié ?"
              color="white"
              textColor="primary"
              onPress={() => navigation.navigate("ResetPassword")}
            />
          </Form>
          {/* <AppTextInput placeholder="Email" />
          <AppTextInput placeholder="Mot de passe" type="password" />
          <Button
            title="Se connecter"
            onPress={() => navigation.navigate("Mes charges")}
          />
          <Button
            title="Mot de passe oublié ?"
            color="white"
            textColor="primary"
          /> */}
        </View>
      </Container>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
  mainText: {
    width: "80%",
  },
  form: {
    width: "100%",
    paddingHorizontal: 5,
  },
});
export default LoginScreen;
