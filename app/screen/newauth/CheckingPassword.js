import React, { useEffect, useState } from "react";
import { StyleSheet, View, Keyboard } from "react-native";
import styled from "styled-components";
import * as Yup from "yup";

import AppText from "../../components/AppText";
import Button from "../../components/Button";
import colors from "../../config/colors";
import Screen from "../../components/Screen";

import {
  ErrorMessage,
  Form,
  FormField,
  FormRadioField,
  SubmitButton,
} from "../../components/forms";
import useAuth from "../../auth/useAuth";
import authApi from "../../api/auth";

const Container = styled.View`
  flex: 1;
  padding-horizontal: 20px;
  justify-content: center;
  align-items: center;
`;

const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  text-align: center;

  ${(props) =>
    props.primary &&
    `
    color: ${colors.primary}
  `}
`;

const DescBox = styled.View`
  width: 80%;
  margin-vertical: 20px;
`;

const Desc = styled(AppText)`
  text-align: center;
  font-size: 15px;
`;

const validationSchema = Yup.object().shape({
  password: Yup.string().required("Ce champ est requis"),
});

const CheckingPassword = ({ navigation, route }) => {
  const auth = useAuth();
  const { user } = route.params;
  const [isVisible, setVisible] = useState(true);
  const [loading, setLoading] = useState(false);
  const [loginFailed, setLoginFailed] = useState(false);

  const handleCheck = async (password, session) => {
    const result = await authApi.checkPassword(user.data?.email, password);
    setLoading(false);
    if (result?.data?.user === 0) return setLoginFailed(true);
    setLoginFailed(false);
    console.log("ss", session);
    if (result?.data?.forcepass === "oui") {
      const user = {
        data: result?.data?.user,
      };
      // Rediriger l'user vers la page de changement mot de passe avec OTP
      navigation.navigate("CheckingOtp", { user });
    } else {
      auth.logIn(result?.data?.user, session);
      // navigation.navigate("ChooseProgram", { user: realUser, session });
    }
  };

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", show);
    Keyboard.addListener("keyboardDidHide", remove);

    return () => {
      Keyboard.addListener("keyboardDidShow", show);
      Keyboard.addListener("keyboardDidHide", remove);
    };
  }, []);

  const show = () => {
    setVisible(false);
  };
  const remove = () => {
    setVisible(true);
  };
  return (
    <Screen>
      <Container>
        <View
          style={[styles.mainText, { display: isVisible ? "flex" : "none" }]}
        >
          <Title>
            Accédez à votre proprieté en toute <Title primary>QUIÉTUDE</Title>
          </Title>
        </View>
        <DescBox>
          {!loginFailed && (
            <Desc>
              Bienvenue sur l'application de gestion du syndic de copropriété.
              Veuillez-vous identifier pour y accéder.
            </Desc>
          )}
          <ErrorMessage
            error="Le mot de passe est incorrect."
            visible={loginFailed}
          />
        </DescBox>
        <View style={styles.form}>
          <Form
            initialValues={{ password: "", session: false }}
            onSubmit={({ password, session }) => {
              setLoading(true);
              handleCheck(password, session);
            }}
            validationSchema={validationSchema}
          >
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              name="password"
              placeholder="Mot de passe"
              secureTextEntry
              textContentType="password"
            />
            <FormRadioField
              name="session"
              placeholder="Garder ma session active"
              withRow={true}
            />
            <SubmitButton title="CONNEXION" loading={loading} />
            <Button
              title="Mot de passe oublié ?"
              color="white"
              textColor="primary"
              onPress={() => navigation.navigate("ResetPassword")}
            />
          </Form>
        </View>
      </Container>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
  mainText: {
    width: "80%",
  },
  form: {
    width: "100%",
    paddingHorizontal: 5,
  },
});
export default CheckingPassword;
