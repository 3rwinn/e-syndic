import React, { useContext } from "react";
import {
  View,
  Text,
  StyleSheet,
  Pressable,
  FlatList,
  Image,
} from "react-native";
import styled from "styled-components";

import colors from "../../config/colors";
import Screen from "../../components/Screen";

import { MaterialCommunityIcons } from "@expo/vector-icons";
import useAuth from "../../auth/useAuth";
import { AppContext } from "../../context/AppState";

const Header = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const SubHeader = styled.View`
  align-items: center;
  margin-vertical: 20px;
`;

const Item = ({ imageUrl, title, event, id }) => {
  return (
    <Pressable style={styles.item} onPress={() => event(id, title)}>
      <Image style={styles.programImg} source={{ uri: imageUrl }} />
      <Text numberOfLines={2} style={styles.title}>
        {title}
      </Text>
    </Pressable>
  );
};

const ChooseProgrammeScreen = ({ navigation }) => {
  const { user, logOut } = useAuth();

  // console.log("user", user);

  const { setProgram } = useContext(AppContext);

  // console.log("setProg", setProgram);

  function handleSelectProgram(id) {
    setProgram(id);
    // setTimeout(() => {
    navigation.navigate("App");
    // }, 3000);
    // alert("ID: " + id);
  }

  return (
    <Screen>
      <View style={styles.content}>
        <Header>
          <View>
            <Text style={styles.title}>Bienvenue,</Text>
            <Text style={styles.subtitle}>
              {user?.name} {user?.prenoms}
            </Text>
          </View>
          <Pressable
            onPress={() => {
              logOut();
              // navigation.navigate()
            }}
          >
            <MaterialCommunityIcons
              name="logout"
              color={colors.textLight}
              size={30}
            />
          </Pressable>
        </Header>
        <SubHeader>
          <Text
            style={[
              styles.title,
              {
                color: colors.primary,
                fontSize: 20,
                fontFamily: "Montserrat_900Black",
              },
            ]}
          >
            Liste de vos programmes
          </Text>
          <Text
            style={[
              styles.subtitle,
              {
                color: colors.text,
                fontWeight: "normal",
                // fontFamily: "Montserrat_300Light",
              },
            ]}
          >
            Choisissez un programme
          </Text>
        </SubHeader>
        {/* <View></View> */}
        {/* <List
          mode="ligne"
          datas={formatProg(programmes)}
          event={handleProgramSelection}
        /> */}
        <FlatList
          data={user?.programmes}
          renderItem={({ item }) => (
            <Item
              imageUrl={item.photopgram}
              title={item.libelle}
              id={item.id}
              event={handleSelectProgram}
            />
          )}
          keyExtractor={(item) => item.id.toString()}
          horizontal={false}
        />
      </View>
    </Screen>
  );
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    // fontFamily: "Montserrat_900Black",
    color: colors.text,
    marginBottom: 5,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: "bold",
    // fontFamily: "Montserrat_300Light",
    color: colors.textLight,
  },
  item: {
    marginRight: 10,
    marginBottom: 15,
    width: "100%",
  },
  programImg: {
    // width: 160,
    width: "100%",
    height: 160,
    borderRadius: 5,
    overflow: "hidden",
  },
  title: {
    color: colors.black,
    fontWeight: "bold",
    fontSize: 18,
    marginTop: 5,
    // fontFamily: "Montserrat_900Black",
  },
});

export default ChooseProgrammeScreen;
