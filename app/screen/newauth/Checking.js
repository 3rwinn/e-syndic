import React, { useEffect, useState } from "react";
import { StyleSheet, View, Keyboard } from "react-native";
import styled from "styled-components";
import * as Yup from "yup";

import AppText from "../../components/AppText";
import colors from "../../config/colors";
import Screen from "../../components/Screen";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../../components/forms";
import authApi from "../../api/auth";

const Container = styled.View`
  flex: 1;
  padding-horizontal: 20px;
  justify-content: center;
  align-items: center;
`;

const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  text-align: center;

  ${(props) =>
    props.primary &&
    `
    color: ${colors.primary};
  `}
`;

const DescBox = styled.View`
  width: 80%;
  margin-vertical: 20px;
`;

const Desc = styled(AppText)`
  text-align: center;
  font-size: 15px;
`;

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required("Ce champs est requis")
    .email("Merci d'indiquer une adresse email valide"),
  // password: Yup.string().required("Ce champ est requis"),
});

const Checking = ({ navigation }) => {
  const [isVisible, setVisible] = useState(true);
  const [loading, setLoading] = useState(false);
  const [loginFailed, setLoginFailed] = useState(false);

  const handleCheck = async (email) => {
    const result = await authApi.checkEmail(email);
    setLoading(false);

    // console.log("result", result.data);
    
    if (result.data.user === 0) return setLoginFailed(true);
    setLoginFailed(false);
    const user = {
      data: result.data.user,
    };
    if (result.data.forcepass === "oui") {
      // Rediriger l'user vers la page de changement mot de passe avec OTP
      navigation.navigate("CheckingOtp", { user });
    } else {
      // Rediriger l'user vers la page avec mot de passe
      navigation.navigate("CheckingPassword", { user });
    }

  };

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", show);
    Keyboard.addListener("keyboardDidHide", remove);

    return () => {
      Keyboard.addListener("keyboardDidShow", show);
      Keyboard.addListener("keyboardDidHide", remove);
    };
  }, []);

  const show = () => {
    setVisible(false);
  };
  const remove = () => {
    setVisible(true);
  };
  return (
    <Screen>
      <Container>
        <View
          style={[styles.mainText, { display: isVisible ? "flex" : "none" }]}
        >
          <Title>
            Accédez à votre proprieté en toute <Title primary>QUIÉTUDE</Title>
          </Title>
        </View>
        <DescBox>
          {!loginFailed && (
            <Desc>
              Bienvenue sur l'application de gestion du syndic de copropriété.
              Veuillez-vous identifier pour y accéder.
            </Desc>
          )}
          <ErrorMessage
            error="L'adresse email indiquée n'existe pas dans notre système."
            visible={loginFailed}
          />
        </DescBox>
        <View style={styles.form}>
          <Form
            initialValues={{ email: "" }}
            onSubmit={({ email }) => {
              setLoading(true);
              handleCheck(email);
            }}
            validationSchema={validationSchema}
          >
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              name="email"
              placeholder="Adresse email"
              textContentType="emailAddress"
            />
            
            <SubmitButton title="SUIVANT" loading={loading} />
           
          </Form>
        </View>
      </Container>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
  mainText: {
    width: "80%",
  },
  form: {
    width: "100%",
    paddingHorizontal: 5,
  },
});
export default Checking;
