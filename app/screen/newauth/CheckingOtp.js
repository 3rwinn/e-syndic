import React, { useEffect, useState } from "react";
import { StyleSheet, View, Keyboard } from "react-native";
import styled from "styled-components";
import * as Yup from "yup";

import AppText from "../../components/AppText";
import Button from "../../components/Button";
import colors from "../../config/colors";
import Screen from "../../components/Screen";

import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../../components/forms";
import useAuth from "../../auth/useAuth";
import authApi from "../../api/auth";
import useSnackAlert from "../../snackbar/useSnackAlert";

const Container = styled.View`
  flex: 1;
  padding-horizontal: 20px;
  justify-content: center;
  align-items: center;
`;

const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  text-align: center;

  ${(props) =>
    props.primary &&
    `
    color: ${colors.primary}
  `}
`;

const DescBox = styled.View`
  width: 80%;
  margin-vertical: 20px;
`;

const Desc = styled(AppText)`
  text-align: center;
  font-size: 15px;
`;

const validationSchema = Yup.object().shape({
  otp: Yup.string().required("Ce champs est requis"),
  password: Yup.string()
    .required("Ce champ est requis")
    .notOneOf(
      ["123456", "1234567", "123abc", "123azerty", "azerty", "abc"],
      "Ce mot de passe est trop faible, merci d'en choisir un autre."
    ),
  confirm_password: Yup.string()
    .required("Ce champ est requis")
    .oneOf(
      [Yup.ref("password"), null],
      "Les mots de passe doivent être identiques"
    ),
});

const CheckingOtp = ({ route }) => {
  const auth = useAuth();
  const snack = useSnackAlert();
  const { user } = route.params;
  const [isVisible, setVisible] = useState(true);
  const [loading, setLoading] = useState(false);
  const [loginFailed, setLoginFailed] = useState(false);

  const handleAskOtp = async () => {
    const result = await authApi.reloadOTP(user.data?.email);
    console.log("rea", result);
    if (result.data?.message === "otp send") {
      snack.showAlert(
        "Un nouveau code temporaire (OTP) de six chiffres vient d'etre envoyé par SMS & Mail",
        "success"
      );
    } else {
      snack.showAlert(
        "Une erreur s'est produite, merci de ré-essayer plus tard.",
        "error"
      );
    }
  };

  const handleCheck = async (otp, password) => {
    const result = await authApi.changePasswordWithOTP(
      user.data?.email,
      otp,
      password
    );
    setLoading(false);
    if (result.data?.user === 0) return setLoginFailed(true);
    setLoginFailed(false);

    auth.logIn(result.data?.user);
  };

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", show);
    Keyboard.addListener("keyboardDidHide", remove);

    return () => {
      Keyboard.addListener("keyboardDidShow", show);
      Keyboard.addListener("keyboardDidHide", remove);
    };
  }, []);

  const show = () => {
    setVisible(false);
  };
  const remove = () => {
    setVisible(true);
  };
  return (
    <Screen>
      <Container>
        <View
          style={[styles.mainText, { display: isVisible ? "flex" : "none" }]}
        >
          <Title>
            Accédez à votre proprieté en toute <Title primary>QUIÉTUDE</Title>
          </Title>
        </View>
        <DescBox>
          {!loginFailed && (
            <Desc>
              Merci d'entrer le code de confirmation que vous avez reçu par sms ou email et définissez votre nouveau mot de passe.
            </Desc>
          )}
          <ErrorMessage
            error="Une erreur s'est produite, merci de verifier que le code OTP indiqué est correct."
            visible={loginFailed}
          />
        </DescBox>
        <View style={styles.form}>
          <Form
            initialValues={{ otp: "", password: "", confirm_password: "" }}
            onSubmit={({ otp, password }) => {
              setLoading(true);
              handleCheck(otp, password);
            }}
            validationSchema={validationSchema}
          >
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              name="otp"
              placeholder="Code OTP"
            />
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              name="password"
              placeholder="Nouveau mot de passe"
              secureTextEntry
              textContentType="password"
            />
            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              name="confirm_password"
              placeholder="Confirmation du mot de passe"
              secureTextEntry
              textContentType="password"
            />
            <SubmitButton title="ENREGISTRER" loading={loading} />
            <Button
              title="Pas reçu de code ? cliquer ici"
              color="white"
              textColor="primary"
              onPress={() => handleAskOtp()}
            />
          </Form>
        </View>
      </Container>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
  mainText: {
    width: "80%",
  },
  form: {
    width: "100%",
    paddingHorizontal: 5,
  },
});
export default CheckingOtp;
