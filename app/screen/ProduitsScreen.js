import React from "react";
import { FlatList, View } from "react-native";
import { Container, EmptyContent, Text } from "../config/styles";
import styled from "styled-components";
import AppTextInput from "../components/TextInput";
import AppModal from "../components/Modal";
import colors from "../config/colors";
import useModal from "../hooks/useModal";
import ItemDetails from "../components/conciergerie/ItemDetails";
import useAuth from "../auth/useAuth";
import useApi from "../hooks/useApi";
import useData from "../hooks/useData";

import conciergerie from "../api/conciergerie";
import AppText from "../components/AppText";
import { formatToMoney, stripString } from "../utils/helpers";
import ItemLoader from "../components/conciergerie/ItemLoader";
import { AppContext } from "../context/AppState";

const SearchBox = styled.View`
  padding-horizontal: 20px;
  padding-vertical: 10px;
`;

const Item = styled.TouchableOpacity`
  flex-direction: row;
  margin: 20px;
`;
const ItemImg = styled.Image`
  border-radius: 5px;
  height: 80px;
  width: 80px;
`;
const ItemContent = styled.View`
  padding-horizontal: 20px;
  width: 80%;
`;
const ItemSeparator = styled.View`
  height: 1px;
  width: 100%;
  background-color: ${colors.light};
`;

const ProduitsScreen = () => {
  const itemDetail = useModal();

  const { user } = useAuth();
  const { selectedProgram } = React.useContext(AppContext);

  const getProductsApi = useApi(conciergerie.loadProducts);

  const [loaded, setLoaded] = React.useState(false);
  React.useEffect(() => {
    getProductsApi.request(user.id, selectedProgram);
    setLoaded(true);
  }, []);

  const products = useData(getProductsApi.data, getProductsApi.data.datas);

  const [refresh, setRefreshing] = React.useState(false);

  // produit data
  const [productsData, setProductsData] = React.useState(null);
  React.useEffect(() => {
    if (products.data && loaded) {
      setProductsData(products.data.produits);
      setLoaded(false);
    }
  }, [products, loaded]);

  // Search module
  const handleSearch = (text) => {
    const formatedText = text.toLowerCase();
    if (formatedText !== "" && productsData) {
      const newdata = productsData.filter((prod) =>
        prod.libelle.toLowerCase().includes(formatedText)
      );

      newdata.length > 0
        ? setProductsData(newdata)
        : setProductsData(products.data.produits);
    } else if (formatedText === "") {
      setProductsData(products.data.produits);
    }
  };

  return (
    <Container>
      <SearchBox>
        <AppTextInput
          bg="white"
          radius={30}
          icon="search-web"
          placeholder="Rechercher"
          onChangeText={(text) => handleSearch(text)}
        />
      </SearchBox>

      {getProductsApi.loading ? (
        <ItemLoader />
      ) : productsData !== null && productsData.length === 0 ? (
        <EmptyContent
          message="Aucuns produits pour le moment"
          retry={() => getProductsApi.request(user.id, selectedProgram)}
        />
      ) : (
        productsData !== null && (
          <FlatList
            style={{ backgroundColor: "white" }}
            data={productsData}
            refreshing={refresh}
            onRefresh={() => getProductsApi.request(user.id, selectedProgram)}
            keyExtractor={(item) => item.id.toString()}
            ItemSeparatorComponent={ItemSeparator}
            renderItem={({ item }) => (
              <Item
                key={item.id}
                onPress={() =>
                  itemDetail.toggleModal(
                    "Produit",
                    <ItemDetails
                      product_id={item.id}
                      titre={item.libelle}
                      category={item.cats.libelle}
                      description={item.newdescription}
                      prix={formatToMoney(item.prix)}
                    />
                  )
                }
              >
                <ItemImg source={{ uri: item.img }} />
                <ItemContent>
                  <Text size={14} bold>
                    {item.libelle}
                  </Text>
                  <Text color="text" bold>
                    {item.cats.libelle}
                  </Text>
                  <Text size={13} numberOfLines={2}>
                    {stripString(item.newdescription)}
                  </Text>
                  <Text size={13} color="primary" bold>
                    {formatToMoney(item.prix)} FCFA
                  </Text>
                </ItemContent>
              </Item>
            )}
          />
        )
      )}

      <AppModal
        visible={itemDetail.modalVisible}
        content={itemDetail.modalContent}
        title={itemDetail.modalTitle}
        toggleModal={itemDetail.toggleModal}
      />
    </Container>
  );
};
export default ProduitsScreen;
