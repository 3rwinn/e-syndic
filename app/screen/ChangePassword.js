import React, { useState } from "react";
import { StyleSheet, View } from "react-native";

import styled from "styled-components";

import AppText from "../components/AppText";
import colors from "../config/colors";
import Screen from "../components/Screen";

import * as Yup from "yup";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import useAuth from "../auth/useAuth";
import authApi from "../api/auth";
import { Feather } from "@expo/vector-icons";
import useSnackAlert from "../snackbar/useSnackAlert";

const Container = styled.View`
  flex: 1;
  padding-horizontal: 20px;
  justify-content: center;
  align-items: center;
`;

const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  text-align: center;

  ${(props) =>
    props.primary &&
    `
    color: ${colors.primary}
  `}
`;

const DescBox = styled.View`
  width: 100%;
  margin-vertical: 20px;
`;

const Desc = styled(AppText)`
  text-align: center;
  font-size: 15px;
`;

const validationSchema = Yup.object().shape({
  password1: Yup.string()
    .required("Ce champ est requis.")
    .min(6, "Le mot de passe doit contenir 6 caractères"),
  password2: Yup.string()
    .required("Ce champ est requis")
    .oneOf(
      [Yup.ref("password1"), null],
      "Les mots de passe doivent être identiques"
    ),
});

const ChangePassword = ({ route }) => {
  const auth = useAuth();
  const snack = useSnackAlert();

  const { user } = route.params;
  console.log(user?.old_password);
  const [resetMessage, setResetMessage] = useState("");
  const [resetFailed, setResetFailed] = useState("");
  const [loading, setLoading] = useState(false)

  const handleSubmit = async ({ password1, password2 }) => {
    setLoading(true);
    const result = await authApi.changeDefaultPassword(
      user.data?.id,
      user.old_password,
      password1,
      password2
    );
    setLoading(false);
    if (result.data.user === 0) {
      // setResetFailed(false);
      // setResetMessage(result.data.message);
      snack.showAlert(result.data.message, "error");
    } else {
      snack.showAlert(
        "Nouveau mot de passe enregistrée avec succès",
        "success"
      );
      auth.logIn(user.data);
    }
  };

  return (
    <Screen>
      <Container>
        <View style={[styles.mainText]}>
          <Title>
            <Feather name="lock" size={60} color="black" />
          </Title>
          <Title>Sécuriser votre compte</Title>
        </View>
        <DescBox>
          {resetMessage === "" ? (
            <Desc>
              Vous utilisez actuellement le mot de passe par défaut, merci de
              définir un nouveau mot de passe pour sécuriser votre compte.
            </Desc>
          ) : resetFailed ? (
            <ErrorMessage error={resetMessage} visible={resetFailed} />
          ) : (
            <Desc>{resetMessage}</Desc>
          )}
        </DescBox>
        <View style={styles.form}>
          <Form
            initialValues={{ password1: "", password2: "" }}
            onSubmit={handleSubmit}
            validationSchema={validationSchema}
          >
            <FormField
              autoCapitalize="none"
              name="password1"
              placeholder="Nouveau mot de passe"
              textContentType="password"
              secureTextEntry
            />
            <FormField
              autoCapitalize="none"
              name="password2"
              placeholder="Confirmation du mot de passe"
              textContentType="password"
              secureTextEntry
            />
            <SubmitButton title="Enregistrer" loading={loading} />
          </Form>
        </View>
      </Container>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
  mainText: {
    width: "80%",
  },
  form: {
    width: "100%",
    paddingHorizontal: 5,
  },
});
export default ChangePassword;
