import React, { useContext } from "react";
import { FlatList, View, RefreshControl } from "react-native";

import {
  Container,
  Section,
  SectionBody,
  SectionTitle,
  Statistiques,
  StatBox,
  StatTitle,
  StatDesc,
  Devise,
  EmptyContent,
  shadowStyleIOS,
} from "../config/styles";
import ChargeItem from "../components/charge/ChargeItem";
import AppModal from "../components/Modal";
import usePaymentMethod from "../hooks/usePaymentMethod";
import useApi from "../hooks/useApi";
import chargeApi from "../api/charge";
import useAuth from "../auth/useAuth";
import { formatToMoney } from "../utils/helpers";
import ChargeLoader from "../components/charge/ChargeLoader";
import { AppContext } from "../context/AppState";

const ExceptionnelleScreen = () => {
  const [modalVisible, setModalVisible] = React.useState(false);
  function toggleModal() {
    setModalVisible(!modalVisible);
  }
  const [data, setData] = React.useState(null);
  const modalData = usePaymentMethod(data);
  const showModal = (data) => {
    // console.log("DEDE", data);
    setData({
      real: data,
      type: "autre",
    });
    toggleModal();
  };

  // Load user charges
  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  const [chargeData, setChargeData] = React.useState(null);
  const chargeApiData = useApi(chargeApi.loadUserCharges);

  React.useEffect(() => {
    chargeApiData.request(user.id, selectedProgram);
  }, []);

  React.useEffect(() => {
    if (chargeApiData.data.datas) {
      setChargeData(chargeApiData.data.datas.chargexceptionnels);
    }
  }, [chargeApiData.data]);

  // Refresh and reset
  const [refreshing, setRefresh] = React.useState(false);
  function reset() {
    modalData.changePaymentMethod("");
    chargeApiData.request(user.id, selectedProgram);
  }

  return (
    <Container>
      <View>
        {chargeApiData.loading ? (
          <ChargeLoader />
        ) : chargeData !== null && chargeData.length === 0 ? (
          <EmptyContent
            message="Aucunes charges pour le moment"
            retry={() => chargeApiData.request(user.id)}
          />
        ) : (
          chargeData && (
            <FlatList
              refreshing={refreshing}
              onRefresh={() => chargeApiData.request(user.id, selectedProgram)}
              ListHeaderComponent={
                <>
                  <Section>
                    <SectionBody>
                      <SectionTitle>Rapport général</SectionTitle>
                      {chargeData && (
                        <Statistiques>
                          <StatBox style={shadowStyleIOS}>
                            <Devise>FCFA</Devise>
                            <StatTitle>
                              {formatToMoney(chargeData.montantpayeexcptionnel)}
                            </StatTitle>
                            <StatDesc>Charges exceptionnelles payées</StatDesc>
                          </StatBox>

                          <StatBox style={shadowStyleIOS}>
                            <Devise>FCFA</Devise>
                            <StatTitle>
                              {formatToMoney(
                                chargeData.montantimpayeexcptionnel
                              )}
                            </StatTitle>
                            <StatDesc>
                              Charges exceptionnelles impayées
                            </StatDesc>
                          </StatBox>
                        </Statistiques>
                      )}
                    </SectionBody>
                  </Section>
                  <Section>
                    <SectionBody>
                      <SectionTitle>Mes charges exceptionnelles</SectionTitle>
                    </SectionBody>
                  </Section>
                </>
              }
              data={chargeData.listechargexcptionnel}
              keyExtractor={(item) => item.id.toString()}
              // refreshing={refreshing}
              // onRefresh={() => chargeApiData.request(user.id)}
              renderItem={({ item }) => (
                <ChargeItem
                  onPress={() => {
                    item.status === 0 ? showModal(item) : null;
                  }}
                  titre={item.imprevus.titre}
                  description={`Lot: ${item.lot.lot}`}
                  status={item.status === 0 ? "non payé" : "payé"}
                  montant={formatToMoney(item.montant)}
                  date={item.datefin}
                />
              )}
            />
          )
        )}
      </View>
      <AppModal
        visible={modalVisible}
        toggleModal={toggleModal}
        title="Paiement"
        content={modalData.modalContent}
        reset={reset}
      />
    </Container>
  );
};

export default ExceptionnelleScreen;
