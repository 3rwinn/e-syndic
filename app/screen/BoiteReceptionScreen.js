import React, { useState, useEffect, useContext } from "react";
import { FlatList, View } from "react-native";
import useAuth from "../auth/useAuth";
import message from "../api/message";

import MessageDetail from "../components/message/MessageDetail";
import MessageItem from "../components/message/MessageItem";
import AppModal from "../components/Modal";
import { Container, EmptyContent } from "../config/styles";
import useApi from "../hooks/useApi";
import useData from "../hooks/useData";
import MessageLoader from "../components/message/MessageLoader";
import { AppContext } from "../context/AppState";
// import { EmptyContent } from "./AgEncoursScreen";

const BoiteReceptionScreen = () => {
  const [refresh, setRefreshing] = useState(false);
  const messageModal = useModal();

  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  const messagesReceivedApi = useApi(message.loadUserMessagesReceived);
  useEffect(() => {
    messagesReceivedApi.request(user.id, selectedProgram);
  }, []);
  const messagesReceived = useData(
    messagesReceivedApi.data,
    messagesReceivedApi.data.datas
  );
  return (
    <Container>
      {messagesReceivedApi.loading ? (
        <MessageLoader />
      ) : messagesReceived.data === null ||
        (messagesReceived.data !== null &&
          messagesReceived.data.length === 0) ? (
        <EmptyContent
          message="Aucuns messages pour le moment."
          retry={() => messagesReceivedApi.request(user.id)}
        />
      ) : (
        <FlatList
          refreshing={refresh}
          onRefresh={() =>
            messagesReceivedApi.request(user.id, selectedProgram)
          }
          data={messagesReceived.data}
          style={{ flex: 1, paddingTop: 20 }}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <View style={{ marginBottom: 20, marginHorizontal: 20 }}>
              <MessageItem
                onPress={() =>
                  messageModal.toggleModal(
                    "Contenu du message",
                    <MessageDetail
                      destinataire={item.from_user.name}
                      date={item.getmessages.created_at}
                      objet={item.getmessages.objet}
                      contenu={item.newcontent}
                      picture={item.auteur_img}
                      // documents={item.documents}
                      mode="in"
                    />
                  )
                }
                destinataire={item.from_user.name}
                date={item.getmessages.created_at}
                objet={item.getmessages.objet}
                contenu={item.newcontent}
                picture={item.auteur_img}
                mode="in"
              />
            </View>
          )}
        />
      )}

      <AppModal
        visible={messageModal.modalVisible}
        toggleModal={messageModal.toggleModal}
        title={messageModal.modalTitle}
        content={messageModal.modalContent}
      />
    </Container>
  );
};

export default BoiteReceptionScreen;
