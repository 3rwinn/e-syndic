import React, { useContext } from "react";
import { FlatList, View } from "react-native";
import {
  Container,
  Section,
  SectionBody,
  SectionTitle,
} from "../config/styles";
import ChargeItem from "../components/charge/ChargeItem";
import useApi from "../hooks/useApi";
import chargeApi from "../api/charge";
import useAuth from "../auth/useAuth";
import { formatToMoney } from "../utils/helpers";
import { AppContext } from "../context/AppState";

const TransactionsScreen = () => {
  // Load user charges
  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  const [chargeData, setChargeData] = React.useState(null);
  const chargeApiData = useApi(chargeApi.loadUserCharges);
  React.useEffect(() => {
    chargeApiData.request(user.id, selectedProgram);
  }, []);

  React.useEffect(() => {
    if (chargeApiData.data.datas) {
      setChargeData(chargeApiData.data.datas.transactions);
    }
  }, [chargeApiData.data]);

  // refresh
  const [refreshing, setRefresh] = React.useState(false);

  return (
    <Container>
      <View>
        {chargeData && (
          <FlatList
            data={chargeData}
            refreshing={refreshing}
            onRefresh={() => chargeApiData.request(user.id, selectedProgram)}
            ListHeaderComponent={
              <>
                <Section>
                  <SectionBody>
                    <SectionTitle>Mes transactions</SectionTitle>
                  </SectionBody>
                </Section>
              </>
            }
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => (
              <ChargeItem
                titre={item.libelle_trasac}
                description={`Ilot: ${item.lot.ilot} / lot: ${item.lot.lot}`}
                montant={formatToMoney(item.montant)}
                date={item.created_at}
              />
            )}
          />
        )}
      </View>
    </Container>
  );
};

export default TransactionsScreen;
