import React from "react";
import Screen from "../components/Screen";
import TopBar from "../components/TopBar";
import { WebView } from "react-native-webview";
import { Container } from "../config/styles";
import { TouchableOpacity } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import * as WebBrowser from "expo-web-browser";

import styled from "styled-components";
import colors from "../config/colors";

const Text = styled.Text`
  font-size: 14px;
  color: ${colors.white};
`;

const GuideScreen = ({ navigation }) => {
  return (
    <Screen>
      <TopBar
        title="Guide d'utilisation"
        navigation={navigation}
        specialBtn={
          <TouchableOpacity
            onPress={async () =>
              await WebBrowser.openBrowserAsync(
                "https://e-syndic.inoovim.com/assets/user_guide_esyndic_v2.pdf"
              )
            }
          >
            <FontAwesome name="download" size={25} color={colors.white} />
          </TouchableOpacity>
        }
      />
      <Container>
        <WebView
          source={{
            uri: "https://e-syndic.inoovim.com/guide_user",
          }}
          scalesPageToFit
          style={{ flex: 1, marginTop: 5 }}
          javaScriptEnabled
        />
      </Container>
    </Screen>
  );
};

export default GuideScreen;
