import React, { useEffect, useState, useContext } from "react";
import { FlatList, TouchableOpacity, View } from "react-native";
import styled from "styled-components";

import AppModal from "../components/Modal";
import colors from "../config/colors";
import IncidentDetail from "../components/incidents/IncidentDetail";
import IncidentItem from "../components/incidents/IncidentItem";
import Screen from "../components/Screen";
import SignalerIncident from "../components/incidents/SignalerIncident";
import TopBar from "../components/TopBar";
import useModal from "../hooks/useModal";
import { Container, EmptyContent } from "../config/styles";
import useAuth from "../auth/useAuth";
import incidentApi from "../api/incident";
import useApi from "../hooks/useApi";
import useData from "../hooks/useData";
import { stripString } from "../utils/helpers";
import IncidentLoader from "../components/incidents/IncidentLoader";
import { AppContext } from "../context/AppState";

const Text = styled.Text`
  font-size: 14px;
  color: ${colors.white};
`;

const IncidentsScreen = ({ navigation }) => {
  const incidentModal = useModal();

  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);

  const loadIncidentApi = useApi(incidentApi.loadUserCityIncidents);
  useEffect(() => {
    loadIncidentApi.request(user.id, selectedProgram);
  }, []);
  const incidents = useData(loadIncidentApi.data, loadIncidentApi.data.datas);

  const [refresh, setRefreshing] = useState(false);

  return (
    <Screen>
      <TopBar
        title="Incidents"
        navigation={navigation}
        specialBtn={
          <TouchableOpacity
            onPress={
              () => navigation.push("SignalerIncident")
              // incidentModal.toggleModal(
              //   "Signaler un incident",
              //   <SignalerIncident onClose={() => incidentModal.close()} />
              // )
            }
          >
            <Text>Signaler un incident</Text>
          </TouchableOpacity>
        }
      />
      <Container>
        {loadIncidentApi.loading ? (
          <IncidentLoader />
        ) : incidents.data === null ||
          (incidents.data !== null && incidents.data.length === 0) ? (
          <EmptyContent
            message="Aucun incident pour le moment."
            retry={() => loadIncidentApi.request(user.id)}
          />
        ) : (
          incidents.data && (
            <FlatList
              style={{ paddingTop: 20 }}
              refreshing={refresh}
              onRefresh={() => loadIncidentApi.request(user.id, selectedProgram)}
              data={incidents.data}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => (
                <View style={{ marginBottom: 20, marginHorizontal: 20 }}>
                  <IncidentItem
                    onPress={() => {
                      incidentModal.toggleModal(
                        "Détail de l'incident",
                        <IncidentDetail
                          author={item.author.name}
                          picture={item.auteur_img}
                          date={item.created_at}
                          status={item.libelle_status}
                          text={item.newdescription}
                          images={item.fichiers}
                          furl={item.urlfile}
                        />
                      );
                    }}
                    key={item.id}
                    author={`${item.author.name} ${item.author.prenoms}`}
                    picture={item.auteur_img}
                    date={item.created_at}
                    status={item.libelle_status}
                    text={stripString(item.newdescription)}
                    images={item.fichiers}
                  />
                </View>
              )}
            />
          )
        )}

        <AppModal
          visible={incidentModal.modalVisible}
          toggleModal={incidentModal.toggleModal}
          title={incidentModal.modalTitle}
          content={incidentModal.modalContent}
        />
      </Container>
    </Screen>
  );
};

export default IncidentsScreen;
