import React from "react";
import SignalerIncident from "../components/incidents/SignalerIncident";
import Screen from "../components/Screen";
import TopBar from "../components/TopBar";
import { Container } from "../config/styles";

const NewIncident = ({navigation}) => {
  return (
    <Screen>
      <TopBar title="Signaler un incident" navigation={navigation} isFakeModal={true} />
      <Container>
        <SignalerIncident />
      </Container>
    </Screen>
  );
};

export default NewIncident;
