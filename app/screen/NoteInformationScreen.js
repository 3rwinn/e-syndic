import React, { useEffect, useState } from "react";
import { FlatList, StyleSheet, View } from "react-native";
import AppModal from "../components/Modal";
import NoteDetail from "../components/notes/NoteDetail";
import NoteItem from "../components/notes/NoteItem";
import Screen from "../components/Screen";
import TopBar from "../components/TopBar";
import { Container, EmptyContent } from "../config/styles";
import useModal from "../hooks/useModal";
import useAuth from "../auth/useAuth";
import notesApi from "../api/note";
import useApi from "../hooks/useApi";
import useData from "../hooks/useData";

import { stripString } from "../utils/helpers";
import NoteLoader from "../components/notes/NoteLoader";
import { AppContext } from "../context/AppState";

const NoteInformationScreen = ({ navigation }) => {
  const noteModal = useModal();

  const { user } = useAuth();
  const { selectedProgram } = React.useContext(AppContext);
  const loadNotesApi = useApi(notesApi.loadUserCityNotes);
  useEffect(() => {
    loadNotesApi.request(user.id, selectedProgram);
  }, []);

  console.log("lna", loadNotesApi)

  const notes = useData(loadNotesApi.data, loadNotesApi.data.datas);
  const [refresh, setRefreshing] = useState(false);

  return (
    <Screen>
      <TopBar title="Note d'information" navigation={navigation} />
      <Container>
        {loadNotesApi.loading ? (
          <NoteLoader />
        ) : notes?.data === null ||
          (notes?.data !== null && notes.data.length === 0) ? (
          <EmptyContent
            message="Aucune note pour le moment"
            retry={() => loadNotesApi.request(user.id, selectedProgram)}
          />
        ) : (
          notes.data && (
            <FlatList
              style={{ paddingTop: 20 }}
              data={notes.data}
              refreshing={refresh}
              onRefresh={() => loadNotesApi.request(user.id, selectedProgram)}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => (
                <View style={{ marginBottom: 20, marginHorizontal: 20 }}>
                  <NoteItem
                    onPress={() => {
                      noteModal.toggleModal(
                        "Détail de la note d'information",
                        <NoteDetail
                          objet={item.objet}
                          text={item.newcontent}
                          picture={item.auteur_img}
                          date={item.created_at}
                        />
                      );
                    }}
                    objet={item.objet}
                    picture={item.auteur_img}
                    text={stripString(item.newcontent)}
                    date={item.created_at}
                  />
                </View>
              )}
            />
          )
        )}
        <AppModal
          visible={noteModal.modalVisible}
          toggleModal={noteModal.toggleModal}
          title={noteModal.modalTitle}
          content={noteModal.modalContent}
        />
      </Container>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {},
});
export default NoteInformationScreen;
