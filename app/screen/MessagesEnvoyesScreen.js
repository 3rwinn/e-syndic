import React, { useState, useEffect, useContext } from "react";
import { FlatList, View } from "react-native";
import useAuth from "../auth/useAuth";
import MessageDetail from "../components/message/MessageDetail";
import MessageItem from "../components/message/MessageItem";
import AppModal from "../components/Modal";
import { Container, EmptyContent } from "../config/styles";
import useApi from "../hooks/useApi";
import message from "../api/message";

import useModal from "../hooks/useModal";
import useData from "../hooks/useData";
import MessageLoader from "../components/message/MessageLoader";
import { AppContext } from "../context/AppState";

const MessagesEnvoyesScreen = () => {
  const messageModal = useModal();

  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  const messagesSentApi = useApi(message.loadUserMessagesSent);
  useEffect(() => {
    messagesSentApi.request(user.id, selectedProgram);
  }, []);
  const messagesSent = useData(
    messagesSentApi.data,
    messagesSentApi.data.datas
  );

  const [refresh, setRefreshing] = useState(false);

  return (
    <Container>
      {messagesSentApi.loading ? (
        <MessageLoader />
      ) : messagesSent.data === null ||
        (messagesSent.data !== null && messagesSent.data.length === 0) ? (
        <EmptyContent
          message="Aucuns messages pour le moment."
          retry={() => messagesSentApi.request(user.id)}
        />
      ) : (
        <FlatList
          refreshing={refresh}
          onRefresh={() => messagesSentApi.request(user.id, selectedProgram)}
          data={messagesSent.data}
          style={{ flex: 1, paddingTop: 20 }}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <View style={{ marginBottom: 20, marginHorizontal: 20 }}>
              <MessageItem
                onPress={() =>
                  messageModal.toggleModal(
                    "Contenu du message",
                    <MessageDetail
                      destinataire={item.to_user.name}
                      date={item.getmessages.created_at}
                      objet={item.getmessages.objet}
                      contenu={item.newcontent}
                      picture={item.auteur_img}
                      // documents={item.documents}
                      mode="in"
                    />
                  )
                }
                destinataire={item.to_user.name}
                date={item.getmessages.created_at}
                objet={item.getmessages.objet}
                contenu={item.newcontent}
                picture={item.auteur_img}
                mode="out"
              />
            </View>
          )}
        />
      )}
      <AppModal
        visible={messageModal.modalVisible}
        toggleModal={messageModal.toggleModal}
        title={messageModal.modalTitle}
        content={messageModal.modalContent}
      />
    </Container>
  );
};

export default MessagesEnvoyesScreen;
