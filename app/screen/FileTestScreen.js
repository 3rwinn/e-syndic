import React from 'react'
import { StyleSheet, View } from 'react-native'
import Screen from '../components/Screen'
import TopBar from '../components/TopBar'

const FileTestScreen = ({navigation}) => {
    return (
            <Screen>
                <TopBar title="File test" navigation={navigation} />
            </Screen>
            )
}
const styles = StyleSheet.create({
 container: {}
})
export default FileTestScreen