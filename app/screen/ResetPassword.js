import React, { useEffect, useState } from "react";
import { StyleSheet, View } from "react-native";

import styled from "styled-components";

import AppText from "../components/AppText";
import Button from "../components/Button";
import colors from "../config/colors";
import Screen from "../components/Screen";

import * as Yup from "yup";
import {
  ErrorMessage,
  Form,
  FormField,
  SubmitButton,
} from "../components/forms";
import useAuth from "../auth/useAuth";
import authApi from "../api/auth";

const Container = styled.View`
  flex: 1;
  padding-horizontal: 20px;
  justify-content: center;
  align-items: center;
`;

const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  text-align: center;

  ${(props) =>
    props.primary &&
    `
    color: ${colors.primary}
  `}
`;

const DescBox = styled.View`
  width: 80%;
  margin-vertical: 20px;
`;

const Desc = styled(AppText)`
  text-align: center;
  font-size: 15px;
`;

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required("Merci d'indiquer une addresse email valide.")
    .email(),
});

const ResetPassword = ({ navigation }) => {
  // const auth = useAuth();
  const [resetMessage, setResetMessage] = useState("");
  const [resetFailed, setResetFailed] = useState("");
  const [loading, setLoading] = useState(false);
  const [emailToVerify, setEmailToVerify] = useState("");
  const [etape, setEtape] = React.useState(0);

  const handleSubmit = async (values) => {
    if (values.step === 0) {
      setLoading(true);
      const result = await authApi.resetPassword(values.email);
      setLoading(false);
      // console.log(result.data);
      if (result.data.message === "1") {
        setResetFailed(false);
        setResetMessage(
          // `Un mail de réinitialisation de votre mot de passe vous a été envoyé à l'adresse suivante ${email}, merci de le consulter`
          `Un code OTP vous a été envoyé par email et SMS, merci de le consulter`
        );
        // resetForm();
        setEmailToVerify(values.email);
        setEtape(1);
      } else {
        setResetFailed(true);
        setResetMessage(
          "Oups ! aucun compte n'est lié à cet e-mail, veuillez le verifier SVP."
        );
      }
    } else if (values.step === 1) {
      setLoading(true);
      const result = await authApi.checkOtp(emailToVerify, values.otp);
      setLoading(false);

      if (result.data.message === "1") {
        setResetFailed(false);
        setResetMessage("Merci de définir votre nouveau mot de passe");
        setEtape(2);
      } else {
        setResetFailed(true);
        setResetMessage(
          "Code OTP incorrect merci de verifier ou cliquer sur retour pour en générer un nouveau."
        );
      }
    } else if (values.step === 2) {
      console.log("PELO");

      setLoading(true);
      const result = await authApi.changeLostPass(
        emailToVerify,
        values.newpassword
      );
      setLoading(false);

      console.log("res clp", result);

      if (result.data.message === "1") {
        alert("Mot de passe modifié avec succès, veuillez vous connecter");
        navigation.navigate("Connexion");
      } else {
        setResetFailed(true);
        setResetMessage(
          "Une erreur est survenue lors de la réinitialisation du mot passe. Merci de réessayer plus tard."
        );
      }
    }
  };

  return (
    <Screen>
      <Container>
        <View style={[styles.mainText]}>
          <Title>Mot de passe oublié</Title>
        </View>
        <DescBox>
          {resetMessage === "" ? (
            <Desc>
              Merci d'indiquer votre adresse email pour lancer le procesus de
              réinitialisation du mot de passe.
            </Desc>
          ) : resetFailed ? (
            <ErrorMessage error={resetMessage} visible={resetFailed} />
          ) : (
            <Desc>{resetMessage}</Desc>
          )}
        </DescBox>
        <View style={styles.form}>
          {etape === 0 && (
            <Form
              initialValues={{ email: "", step: 0 }}
              onSubmit={handleSubmit}
              validationSchema={validationSchema}
            >
              <FormField
                autoCapitalize="none"
                keyboardType="email-address"
                name="email"
                placeholder="Adresse email"
                textContentType="emailAddress"
              />
              <SubmitButton title="Envoyer" loading={loading} />
              <Button
                title="Annuler"
                color="white"
                textColor="primary"
                onPress={() => navigation.navigate("Connexion")}
              />
            </Form>
          )}
          {etape === 1 && (
            <Form
              initialValues={{ otp: "", step: 1 }}
              onSubmit={handleSubmit}
              // validationSchema={validationSchema}
            >
              <FormField
                autoCapitalize="none"
                keyboardType="numeric"
                name="otp"
                placeholder="Code OTP"
              />
              <SubmitButton title="Envoyer" loading={loading} />
              <Button
                title="Retour"
                color="white"
                textColor="primary"
                onPress={() => setEtape(0)}
              />
            </Form>
          )}
          {etape === 2 && (
            <Form
              initialValues={{ newpassword: "", password2: "", step: 2 }}
              // onSubmit={(values) => console.log("values", values)}
              onSubmit={handleSubmit}
              // validationSchema={validationSchema}
            >
              <FormField
                secureTextEntry
                autoCapitalize="none"
                name="newpassword"
                placeholder="Nouveau mot de passe"
              />
              <FormField
                secureTextEntry
                autoCapitalize="none"
                name="password2"
                placeholder="Confirmer le mot de passe"
              />
              <SubmitButton title="Envoyer" loading={loading} />
              {/* <SubmitButton title="Envoyer" /> */}
              <Button
                title="Retour"
                color="white"
                textColor="primary"
                onPress={() => setEtape(0)}
              />
            </Form>
          )}
        </View>
      </Container>
    </Screen>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
  mainText: {
    width: "80%",
  },
  form: {
    width: "100%",
    paddingHorizontal: 5,
  },
});
export default ResetPassword;
