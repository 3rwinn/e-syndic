import React from "react";
import { FlatList, View } from "react-native";
import { Container, EmptyContent, Text } from "../config/styles";
import styled from "styled-components";
import AppTextInput from "../components/TextInput";
import AppModal from "../components/Modal";
import colors from "../config/colors";
import useModal from "../hooks/useModal";
import ItemDetails from "../components/conciergerie/ItemDetails";
import useAuth from "../auth/useAuth";
import useApi from "../hooks/useApi";
import useData from "../hooks/useData";

import conciergerie from "../api/conciergerie";
import { formatToMoney, stripString } from "../utils/helpers";
import ItemLoader from "../components/conciergerie/ItemLoader";
import { AppContext } from "../context/AppState";

const SearchBox = styled.View`
  padding-horizontal: 20px;
  padding-vertical: 10px;
`;

const Item = styled.TouchableOpacity`
  flex-direction: row;
  margin: 20px;
`;
const ItemImg = styled.Image`
  border-radius: 5px;
  height: 80px;
  width: 80px;
`;
const ItemContent = styled.View`
  padding-horizontal: 20px;
  width: 80%;
`;
const ItemSeparator = styled.View`
  height: 1px;
  width: 100%;
  background-color: ${colors.light};
`;

const ServicesScreen = () => {
  const itemDetail = useModal();

  const { user } = useAuth();
  const { selectedProgram } = React.useContext(AppContext);

  const getServiceApi = useApi(conciergerie.loadProducts);

  const [loaded, setLoaded] = React.useState(false);

  React.useEffect(() => {
    getServiceApi.request(user.id, selectedProgram);
    setLoaded(true);
  }, []);

  const services = useData(getServiceApi.data, getServiceApi.data.datas);

  // console.log(services.data, "Mes services")

  const [refresh, setRefreshing] = React.useState(false);

  // services data
  const [servicesData, setServicesData] = React.useState(null);
  React.useEffect(() => {
    if (services.data && loaded) {
      setServicesData(services.data.services);
      setLoaded(false);
    }
  }, [services, loaded]);

  // search module
  const handleSearch = (text) => {
    const formatedText = text.toLowerCase();
    if (formatedText !== "" && servicesData) {
      const newdata = servicesData.filter((service) =>
        service.libelle.toLowerCase().includes(formatedText)
      );

      newdata.length > 0
        ? setServicesData(newdata)
        : setServicesData(services.data.services);
    } else if (formatedText === "") {
      setServicesData(services.data.services);
    }
  };

  return (
    <Container>
      <SearchBox>
        <AppTextInput
          bg="white"
          radius={30}
          icon="search-web"
          placeholder="Rechercher"
          onChangeText={(text) => handleSearch(text)}
        />
      </SearchBox>

      {getServiceApi.loading ? (
        <ItemLoader />
      ) : servicesData !== null && servicesData.length === 0 ? (
        <EmptyContent
          message="Aucuns services pour le moment"
          retry={() => getServiceApi.request(user.id, selectedProgram)}
        />
      ) : (
        servicesData !== null && (
          <FlatList
            style={{ backgroundColor: "white" }}
            data={servicesData}
            refreshing={refresh}
            onRefresh={() => getServiceApi.request(user.id, selectedProgram)}
            keyExtractor={(item) => item.id.toString()}
            ItemSeparatorComponent={ItemSeparator}
            renderItem={({ item, index }) => (
              <Item
                key={index}
                onPress={() =>
                  itemDetail.toggleModal(
                    "Service",
                    <ItemDetails
                      product_id={item.id}
                      titre={item.libelle}
                      category={item.cats.libelle}
                      description={item.newdescription}
                      mode="service"
                      forfaits={item.newforfait}
                      // prix={formatToMoney(item.prix)}
                    />
                  )
                }
              >
                <ItemImg source={{ uri: item.img }} />
                <ItemContent>
                  <Text size={14} bold>
                    {item.libelle}
                  </Text>
                  <Text color="text" bold>
                    {item.cats.libelle}
                  </Text>
                  <Text size={13} numberOfLines={3}>
                    {stripString(item.newdescription)}
                  </Text>
                  {/* <Text size={13} color="primary" bold>
                    {formatToMoney(item.prix)} FCFA
                  </Text> */}
                </ItemContent>
              </Item>
            )}
          />
        )
      )}

      <AppModal
        visible={itemDetail.modalVisible}
        content={itemDetail.modalContent}
        title={itemDetail.modalTitle}
        toggleModal={itemDetail.toggleModal}
      />
    </Container>
  );
};

export default ServicesScreen;
