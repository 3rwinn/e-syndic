import React, { useEffect, useState, useContext } from "react";
import { FlatList, TouchableOpacity, View } from "react-native";
import styled from "styled-components";
import colors from "../config/colors";
import { Container, EmptyContent } from "../config/styles";
import AppModal from "../components/Modal";
import Screen from "../components/Screen";
import TopBar from "../components/TopBar";
import EvenementItem from "../components/evenements/EvenementItem";
import AddEvenement from "../components/evenements/AddEvenement";
import EvenementDetail from "../components/evenements/EvenementDetail";
import useAuth from "../auth/useAuth";
import evenementApi from "../api/evenement";
import useModal from "../hooks/useModal";
import useApi from "../hooks/useApi";
import useData from "../hooks/useData";
import { stripString } from "../utils/helpers";
import EvenementLoader from "../components/evenements/EvenementLoader";
import { AppContext } from "../context/AppState";

const Text = styled.Text`
  font-size: 14px;
  color: ${colors.white};
`;

const EvenementScreen = ({ navigation }) => {
  const evenementModal = useModal();

  const { user } = useAuth();
  const { selectedProgram } = useContext(AppContext);
  const loadEvenementApi = useApi(evenementApi.loadUserCityEvenements);
  useEffect(() => {
    loadEvenementApi.request(user.id, selectedProgram);
  }, []);
  const evenements = useData(
    loadEvenementApi.data,
    loadEvenementApi.data.datas
  );

  const [refresh, setRefreshing] = useState(false);
  return (
    <Screen>
      <TopBar
        title="Évènements"
        navigation={navigation}
        specialBtn={
          <TouchableOpacity
            onPress={
              () => navigation.push("AddEvenement")
              // evenementModal.toggleModal(
              //   "Créer un événement",
              //   <AddEvenement changeVisibility={evenementModal.setVisible} onClose={() => evenementModal.close()} />
              // )
            }
          >
            <Text>Créer un évènement</Text>
          </TouchableOpacity>
        }
      />
      <Container>
        {loadEvenementApi.loading ? (
          <EvenementLoader />
        ) : evenements.data === null ||
          (evenements.data !== null && evenements.data.length === 0) ? (
          <EmptyContent
            message="Aucuns évènements pour le moment."
            retry={() => loadEvenementApi.request(user.id, selectedProgram)}
          />
        ) : (
          evenements.data && (
            <FlatList
              style={{ paddingTop: 20 }}
              refreshing={refresh}
              onRefresh={() =>
                loadEvenementApi.request(user.id, selectedProgram)
              }
              data={evenements.data}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item }) => (
                <View style={{ marginBottom: 20, marginHorizontal: 20 }}>
                  <EvenementItem
                    onPress={() =>
                      evenementModal.toggleModal(
                        "Détail de l'évènement",
                        <EvenementDetail
                          author={`${item.author.name} ${item.author.prenoms}`}
                          picture={item.auteur_img}
                          datedebut={item.datedebut}
                          datefin={item.datefin}
                          bruit={item.bruit}
                          text={item.newdescription}
                          titre={item.libelle}
                          image={item.new_img}
                        />
                      )
                    }
                    author={`${item.author.name} ${item.author.prenoms}`}
                    picture={item.auteur_img}
                    datedebut={item.datedebut}
                    datefin={item.datefin}
                    bruit={item.bruit}
                    text={stripString(item.newdescription)}
                    titre={item.libelle}
                    image={item.new_img}
                  />
                </View>
              )}
            />
          )
        )}
        <AppModal
          visible={evenementModal.modalVisible}
          toggleModal={evenementModal.toggleModal}
          title={evenementModal.modalTitle}
          content={evenementModal.modalContent}
        />
      </Container>
    </Screen>
  );
};

export default EvenementScreen;
