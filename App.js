import React, { useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
// import AppLoading from "expo-app-loading";
import AppNavigator from "./app/navigation/AppNavigator";
import AuthContext from "./app/auth/context";
import authStorage from "./app/auth/storage";
import SnackContext from "./app/snackbar/context";
import { navigationRef } from "./app/navigation/rootNavigation";
import AuthNavigator from "./app/navigation/AuthNavigator";
import navigationTheme from "./app/navigation/navigationTheme";
import QrNavigator from "./app/navigation/QrNavigator";
// import * as SplashScreen from "expo-splash-screen";
import * as Sentry from "sentry-expo";
import SnackAlert from "./app/snackbar/SnackAlert";
import ProgrammeNavigator from "./app/navigation/ProgramNavigator";
import { AppProvider } from "./app/context/AppState";
Sentry.init({
  dsn: "https://da9f6d2fb74e4fadb484d7eba00342be@o486277.ingest.sentry.io/5542972",
  enableInExpoDevelopment: true,
  debug: false, // Sentry will try to print out useful debugging information if something goes wrong with sending an event. Set this to `false` in production.
});

export default function App() {
  const [user, setUser] = useState();
  const [alert, setAlert] = useState();
  // const [isReady, setIsReady] = useState(false);

  const restoreUser = async () => {
    const user = await authStorage.getUser();
    if (user) setUser(user);
  };

  React.useEffect(() => {
    restoreUser();
  }, []);

  // console.log("user", user);

  const [activeNavigator, setActiveNavigator] = React.useState(
    <AuthNavigator />
  );
  React.useEffect(() => {
    if (user === undefined || user === null) {
      setActiveNavigator(<AuthNavigator />);
    } else {
      setActiveNavigator(<ProgrammeNavigator />);
    }
  }, [user]);

  return (
    <>
      <AppProvider>
        <AuthContext.Provider value={{ user, setUser }}>
          <SnackContext.Provider value={{ alert, setAlert }}>
            <NavigationContainer ref={navigationRef} theme={navigationTheme}>
              {activeNavigator}
              {/* { */}
              {/* // user === undefined || user === null ? ( */}
              {/* // user?.role_id === 3 || user?.role_id == 4 ? ( */}
              {/* // <AppNavigator /> */}
              {/* <AuthNavigator /> */}
              {/* ) : ( */}
              {/* // ) : ( */}
              {/* // <QrNavigator /> */}
              {/* // ) */}
              {/* <ProgrammeNavigator /> */}
              {/* )} */}
            </NavigationContainer>
            <SnackAlert />
          </SnackContext.Provider>
        </AuthContext.Provider>
      </AppProvider>
    </>
  );
}
